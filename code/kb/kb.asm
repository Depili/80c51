	CPU	8051
	INCLUDE	../p80c51fa.inc
	INCLUDE ../paulmon2/paulmon_v3.0.inc

	EQU	leds_key, 'Z'
	EQU	kb_key, 'V'
	EQU	base, 2200h

	EQU	lcd_cmd, 6020h
	EQU	lcd_data, 6021h
	EQU	kb_data, 60FCh
	EQU	leds_1, 60E0h
	EQU	leds_spacing, 4
	EQU	leds_registers, 5

	EQU	leds_buffer, 060h		; 5 bytes

	equ	galois_state, 50h		; 4 bytes of state

	ORG 	base

	db	0A5H,0E5H,0E0H,0A5H		; signature
	db	254,leds_key,0,0		; id (254=user installed command)
	db	0,0,0,0				; prompt code vector
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; user defined
	db	255,255,255,255			; length and checksum (255=unused)
	db	"KB led test",0

	ORG 	base + 40h

start:
	SETB	P3.4
	SETB	P3.5
	MOV	A, #99h
.init:
	MOV	dptr, #leds_1
	MOV	R1, #leds_spacing
	MOV	R2, #leds_registers
.loop:
	MOVX	@dptr, A
	PUSH	ACC
	MOV	A, DPL
	ADD	A, R1
	MOV	DPL, A
	POP	ACC
	XRL	A, #0FFh
	DEC	R2
	CJNE	R2, #0, .loop
	CALL	delay_leds
	CALL	pm.esc
	JC	.escape
	JMP	.init
.escape:
	RET


lcd_start:
	CALL	lcd_init
	MOV	dptr, #string
	CALL	lcd_print
	RET


delay_leds:
	MOV	R3, 0FFh
-	MOV	R4, 0FFh
	DJNZ	R4, $
	DJNZ	R3, -
	RET


delay:
;	MOV 	r1, #0ffh
-	MOV 	r0, #018h
	DJNZ 	r0, $
;	DJNZ 	r1, -
	RET

delay_long:
	MOV 	r1, #00fh
-	MOV 	r0, #0ffh
	DJNZ 	r0, $
	DJNZ 	r1, -
	RET


string:
	DB	"LCD test OK", 0

	ORG 	base+300h

	db	0A5H,0E5H,0E0H,0A5H		; signature
	db	254,kb_key,0,0			; id (254=user installed command)
	db	0,0,0,0				; prompt code vector
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; user defined
	db	255,255,255,255			; length and checksum (255=unused)
	db	"KB test",0

	ORG 	base + 340h

kb_start:
	CLR	RI
	CLR	TF0
	CLR	TR0
	MOV	70, #00h
	MOV	TH0, #0CBh
	MOV	TL0, #0EBh
	MOV	TMOD, #01h
	MOV	TCON, #00h
	SETB	ET0
	SETB	EA
	SETB	TR0
	MOV	DPTR, #kb_data
	MOVX	A, @DPTR
	MOV	R1, A
.loop:
	MOVX	A, @DPTR
	PUSH	ACC
	XRL	A, R1
	JNZ	.changed
	POP	ACC
	JB	RI, .escape
	MOV	R7, 70
	CJNE	R7, #0, .loop
	CALL	clear_leds
	JMP	.loop
.changed:
	POP	ACC
	INC	AUXR1

	CALL	key_to_led
	PUSH	ACC
	MOV	R7, A
	MOV	DPTR, #leds_1
	MOV	A, DPL
	ADD	A, R5
	MOV	DPL, A
	MOV	A, R5
	RR	A
	RR	A
	ADD	A, #leds_buffer
	MOV	R0, A
	MOV	A, R7
	MOV	70, #0E0h
	JB	ACC.7, .down
	SETB	TR0
	MOV	A, #01h
	JMP	.bit_loop
.down:
	CLR	TR0
	MOV	A, #02h
.bit_loop:
	RL	A
	RL	A
	DEC	R6
	CJNE	R6, #0, .bit_loop
	XRL	A, @R0
	MOVX	@DPTR, A
	MOV	@R0, A

	POP	ACC

	JB	ACC.7, .keydown
	MOV	DPTR, #keyup
.print:
	CALL	pm.pstr
	CALL	pm.phex
	CALL	pm.newline
	INC	AUXR1
	MOV	R1, A
	JMP	.loop
.keydown:
	MOV	DPTR, #keydown
	JMP	.print
.escape:
	CLR	EA
	CLR	RI
	RET


keyup:
	DB	"Key up: ",0
keydown:
	DB	"Key down: ", 0

clear_leds:
	PUSH	DPL
	PUSH	DPH
	PUSH	ACC
	MOV	DPTR, #leds_1
	MOV	R7, #leds_registers
	MOV	R0, #leds_buffer
.loop:
	MOV	A, #0FFh
	MOVX	@DPTR, A
	MOV	@R0, A
	MOV	A, DPL
	ADD	A, #leds_spacing
	MOV	DPL, A
	INC	R0
	DJNZ	R7, .loop
	POP	ACC
	POP	DPH
	POP	DPL
	CLR	TR0
	RET

key_to_led:
	PUSH	ACC
	MOV	R5, A
	ANL	A, #03h
	MOV	R6, A		; The in-byte offset
	MOV	A, R5
	ANL	A, #07Ch	; The register offset
	MOV	R5, A
	POP	ACC
	RET

lcd_print:
	INC	AUXR1
	MOV	dptr, #lcd_data
	INC	AUXR1
.loop:
	CLR	A
	MOVC	A, @a+dptr
	INC	AUXR1
	JZ	.end
	MOVX	@dptr, A
	INC	AUXR1
	INC	dptr
	CALL	delay
	SJMP	.loop
.end


	ORG	vector + 0Bh			; Timer 0
isr_timer1:
	INC	70
	RETI


	ORG	base + 500h
	db	0A5H,0E5H,0E0H,0A5H		; signature
	db	254,'A',0,0			; id (254=user installed command)
	db	0,0,0,0				; prompt code vector
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; user defined
	db	255,255,255,255			; length and checksum (255=unused)
	db	"Galois RNG",0
	ORG	base + 540h

galois:
	mov	A, galois_state
	orl	A, galois_state+1
	orl	A, galois_state+2
	orl	A, galois_state+3
	jnz	.process
	; Initialize the LFSR, as 0 is a lock-up state
	mov	galois_state, #42
.process:
	clr	CY
	mov	R0, #galois_state
	mov	R1, #4
.loop:
	mov	A, @R0
	rrc	A
	mov 	@R0, A
	inc	R0
	djnz	R1, .loop
	jc	.carry
	mov	A, #'0'
	call	pm.cout
	jmp	.end
.carry:
	mov	A, #'1'
	call	pm.cout
	mov	A, galois_state
	xrl	A, #0A3h
	mov	galois_state, A
.end:
	ret


lcd_init:
	MOV	dptr, #lcd_cmd
	MOV	A, #38h				; 8 bit interface, 2 rows, 5x8 font
	MOVX	@dptr, A
	CALL	delay_long
	MOV	A, #0Ch				; Display on, cursor off, blink off
	MOVX	@dptr, A
	CALL	delay_long
	MOV	A, #02h				; Home display
	MOVX	@dptr, A
	CALL	delay_long
	MOV	A, #01h				; Clear display
	MOVX	@dptr, A
	CALL	delay_long
	RET
