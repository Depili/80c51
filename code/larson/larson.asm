	CPU	8051
	INCLUDE	../p80c51fa.inc

	ORG 	3000h

	EQU 	esc, 3eh
	EQU	speed, 038h

pwm2	MACRO	pin1, pin2
	CLR 	A
	MOV 	r1, #speed
-	MOV 	r0, #0ffh
-	INC 	A
	CLR 	pin1
	CLR	pin2
	JNB 	ACC.0, +
	SETB 	pin1
	JNB 	ACC.1, +
	SETB 	pin2
+	DJNZ 	r0, -
	DJNZ 	r1, --
	CLR 	pin1
	CLR 	pin2
	ENDM

pwm1	MACRO	pin
	CLR 	A
	MOV 	r1, #speed
-	MOV 	r0, #0ffh
-	INC 	A
	CLR 	pin
	JB 	ACC.0, +
	SETB 	pin
+	DJNZ 	r0, -
	DJNZ 	r1, --
	CLR 	pin
	ENDM


start:
	clr 	p1.7
	clr 	p3.2
	clr 	p3.3
	clr 	p3.4
	clr 	p3.5
	clr	A
loop:
	setb 	p1.7 		; Xx_
	pwm2	p3.2, p3.3
	pwm2	p3.2, p3.2	; X_
	setb	p1.7
	call	delay
	setb 	p3.2		; xX
	pwm1	p1.7
	setb	p3.3		; _xX
	pwm2	p3.2, p1.7
	setb	p3.4		;  _xX
	pwm2	p3.3, p3.2
	setb	p3.5		;   _xX
	pwm2	p3.4, p3.3

	setb	p3.5		;    _X
	pwm2	p3.4, p3.4
	setb	p3.5
	call	delay
	setb	p3.4		;    Xx
	pwm1	p3.5
	setb	p3.3		;   Xx_
	pwm2	p3.4, p3.5
	setb	p3.2		;  Xx_
	pwm2	p3.3, p3.4
	call	esc
	jc	escape
	jmp	loop
escape:
	ret


delay:
	MOV 	r1, #0ffh
-	MOV 	r0, #0ffh
	DJNZ 	r0, $
	DJNZ 	r1, -
	RET
