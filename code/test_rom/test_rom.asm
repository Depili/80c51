	CPU	8051
	INCLUDE	../p80c51fa.inc
	INCLUDE ../paulmon2/paulmon_v3.0.inc

	ORG 	0000h


	EQU	mem_key, 'X'
	EQU 	larson_key, 'W'
	EQU	lcd_key, 'T'

	EQU	speed, 038h
	EQU	base, 3000h

	EQU	lcd_cmd, 6020h
	EQU	lcd_data, 6021h


pwm2	MACRO	pin1, pin2
	CLR 	A
	MOV 	r1, #speed
-	MOV 	r0, #0ffh
-	INC 	A
	CLR 	pin1
	CLR	pin2
	JNB 	ACC.0, +
	SETB 	pin1
	JNB 	ACC.1, +
	SETB 	pin2
+	DJNZ 	r0, -
	DJNZ 	r1, --
	CLR 	pin1
	CLR 	pin2
	ENDM

pwm1	MACRO	pin
	CLR 	A
	MOV 	r1, #speed
-	MOV 	r0, #0ffh
-	INC 	A
	CLR 	pin
	JB 	ACC.0, +
	SETB 	pin
+	DJNZ 	r0, -
	DJNZ 	r1, --
	CLR 	pin
	ENDM


start:
	clr 	p1.7
	clr 	p3.2
	clr 	p3.3
	clr 	p3.4
	clr 	p3.5
	clr	A
.loop:
	setb 	p1.7 		; Xx_
	pwm2	p3.2, p3.3
	pwm2	p3.2, p3.2	; X_
	setb	p1.7
	call	delay
	setb 	p3.2		; xX
	pwm1	p1.7
	setb	p3.3		; _xX
	pwm2	p3.2, p1.7
	setb	p3.4		;  _xX
	pwm2	p3.3, p3.2
	setb	p3.5		;   _xX
	pwm2	p3.4, p3.3

	setb	p3.5		;    _X
	pwm2	p3.4, p3.4
	setb	p3.5
	call	delay
	setb	p3.4		;    Xx
	pwm1	p3.5
	setb	p3.3		;   Xx_
	pwm2	p3.4, p3.5
	setb	p3.2		;  Xx_
	pwm2	p3.3, p3.4
	; call	esc
	jc	.escape
	jmp	.loop
.escape:
	ret


delay:
	MOV 	r1, #0ffh
-	MOV 	r0, #0ffh
	DJNZ 	r0, $
	DJNZ 	r1, -
	RET

	ORG 	0200h
	PHASE 	8200h

	db	0A5H,0E5H,0E0H,0A5H		; signiture
	db	254,larson_key,0,0		; id (254=user installed command)
	db	0,0,0,0				; prompt code vector
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; user defined
	db	255,255,255,255			; length and checksum (255=unused)
	db	"Larson scanner",0

	ORG 	0240h

start2:
	clr 	p1.7
	clr 	p3.2
	clr 	p3.3
	clr 	p3.4
	clr 	p3.5
	clr	A
.loop:
	setb 	p1.7 		; Xx_
	pwm2	p3.2, p3.3
	pwm2	p3.2, p3.2	; X_
	setb	p1.7
	call	delay2
	setb 	p3.2		; xX
	pwm1	p1.7
	setb	p3.3		; _xX
	pwm2	p3.2, p1.7
	setb	p3.4		;  _xX
	pwm2	p3.3, p3.2
	setb	p3.5		;   _xX
	pwm2	p3.4, p3.3

	setb	p3.5		;    _X
	pwm2	p3.4, p3.4
	setb	p3.5
	call	delay2
	setb	p3.4		;    Xx
	pwm1	p3.5
	setb	p3.3		;   Xx_
	pwm2	p3.4, p3.5
	setb	p3.2		;  Xx_
	pwm2	p3.3, p3.4
	call	pm.esc
	jc	.escape
	jmp	.loop
.escape:
	ret


delay2:
	MOV 	r1, #0ffh
-	MOV 	r0, #0ffh
	DJNZ 	r0, $
	DJNZ 	r1, -
	RET


	ORG 	0400h

	db	0A5H,0E5H,0E0H,0A5H		; signature
	db	254,lcd_key,0,0			; id (254=user installed command)
	db	0,0,0,0				; prompt code vector
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; user defined
	db	255,255,255,255			; length and checksum (255=unused)
	db	"LCD test",0

	ORG 	0440h

lcd_test:
	MOV	dptr, #lcd_cmd
	MOV	A, #38h				; 8 bit interface, 2 rows, 5x8 font
	MOVX	@dptr, A
	CALL	cmd_delay
	MOV	A, #0Ch				; Display on, cursor off, blink off
	MOVX	@dptr, A
	CALL	cmd_delay
	MOV	A, #02h				; Home display
	MOVX	@dptr, A
	CALL	cmd_delay
	MOV	A, #01h				; Clear display
	MOVX	@dptr, A
	CALL	cmd_delay
	MOV	dptr, #lcd_data
	INC	AUXR1
	MOV	dptr, #string
.loop:
	CLR	A
	MOVC	A, @a+dptr
	INC	AUXR1
	JZ	.end
	MOVX	@dptr, A
	INC	AUXR1
	INC	dptr
	CALL	chr_delay
	SJMP	.loop
.end
	RET



chr_delay:
;	MOV 	r1, #0ffh
-	MOV 	r0, #018h
	DJNZ 	r0, $
;	DJNZ 	r1, -
	RET

cmd_delay:
	MOV 	r1, #00fh
-	MOV 	r0, #0ffh
	DJNZ 	r0, $
	DJNZ 	r1, -
	RET


string:
	DB	"LCD test OK", 0

	ORG	0600h

	db	0A5H,0E5H,0E0H,0A5H		; signature
	db	254,mem_key,0,0			; id (254=user installed command)
	db	0,0,0,0				; prompt code vector
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; user defined
	db	255,255,255,255			; length and checksum (255=unused)
	db	"SRAM test",0

	ORG 	0640h

mem_test:
	MOV	dptr, #str_ram1
	CALL	pm.pstr
	MOV	dptr, #0000h
	MOV	A, #41h
	MOV	R1, #0e0h
.loop1:
	MOVX	@dptr, A
	MOV	@r1, A
	MOVX	A, @dptr
	CJNE	A, 0e0h, .fail
	INC	dptr
	MOV	A, #20h
	CJNE	A, DPH, .continue1
	SJMP	.ram2
.continue1:
	MOV	A, @r1
	INC	A
	SJMP	.loop1
.fail:
	MOV	dptr, #str_fail
	CALL	pm.pstr
	RET
.ram2:
	MOV	dptr, #str_ok
	CALL	pm.pstr
	CALL	pm.pstr
	MOV	dptr, #2000h
.loop2:
	MOVX	@dptr, A
	MOV	@r1, A
	MOVX	A, @dptr
	CJNE	A, 0e0h, .fail
	CLR	A
	MOVC	A, @a+dptr
	CJNE	A, 0e0h, .fail
	INC	dptr
	MOV	A, #40h
	CJNE	A, DPH, .continue2
	SJMP	.ok
.continue2:
	MOV	A, @r1
	INC	A
	SJMP	.loop2
.ok:
	MOV	dptr, #str_ok
	CALL	pm.pstr
	CALL	pm.newline
	RET



str_ram1:
	DB	"\r\nTesting ram1... ",0


str_fail:
	DB	"Test failed\r\n\n", 0

str_ok:
	DB	"OK\r\n", 0

str_ram2:
	DB	"Testing ram2... ",0