; User installable disassembler and single-step run for paulmon2

; Please email comments, suggestions, bugs to paul@pjrc.com

; This code is in the public domain. It is distributed in
; the hope that it will be useful, but without any warranty;
; without even the implied warranty of merchantability or fitness
; for a particular purpose.

; For more information, please see

; http://www.pjrc.com/tech/8051/pm2_docs/index.html
	CPU	8051
	INCLUDE ../p80c51fa.inc
	INCLUDE paulmon_strings_v3.0.inc
	INCLUDE paulmon_v3.0.inc

	.equ	locat, 01000H			; location for these commands (usually 1000)



; Disassembler
;---------------------------------------------------------;
;                                                         ;
;                      list command                       ;
;                                                         ;
;---------------------------------------------------------;


	org	locat
	db	0A5H,0E5H,0E0H,0A5H		; signiture
	db	254,list_key,0,0		; id (254=user installed command)
	db	0,0,0,0				; prompt code vector
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; user defined
	db	255,255,255,255			; length and checksum (255=unused)
	db	"List",0

newline_h:
	ljmp	pm.newline

	org	locat + 64			; executable code begins here

;	disassembler register usage
;	r0 = temporary storage
;	r1 = temporart storage
;	r2 = first instruction byte
;	r3 = second instruction byte
;	r4 = third instruction byte
;	r5 = line count
;	r6 = program counter (lsb)
;	r7 = program counter (msb)

list:	acall	newline_h
	mov	r5, #20				; Dissassemble 20 instructions
	clr	psw.1				; use ordinary long format
-	acall	disasm
	djnz	r5, -
	ajmp	newline_h

disasm:
; print out the memory location and fetch the next three bytes
	mov	a, r7
	mov	dph, a
	acall	phex_h
	mov	a, r6
	mov	dpl, a
	acall	phex_h
	clr	a
	movc	a, @a+dptr
	mov	r2, a
	inc	dptr
	clr	a
	movc	a, @a+dptr
	mov	r3, a
	inc	dptr
	clr	a
	movc	a, @a+dptr
	mov	r4, a
	mov	a, r2
	anl	a, #00001000b
	jnz	.lookup_Rx

; fetch constants for instructions not using R0-R7
.lookup_no_Rx:
	mov	a, r2
	rr	a
	anl	a, #01111000b			; grab upper 4 bits
	mov	r0, a				; keep in r0 for a moment
	mov	a, r2
	anl	a, #00000111b			; get lower 3 bits
	orl	a, r0				; combine in upper 4
	mov	dptr, #opot1			; opot=operand offset table
	movc	a, @a+dptr
	sjmp	.unpack

; fetch constants for R0-R7 instructions
.lookup_Rx:
	mov	a, r2
	swap	a
	anl	a, #00001111b
	mov	dptr, #opot2
	movc	a, @a+dptr

; now we'll unpack the operand code (# bytes and addr mode)
.unpack:
	anl	a, #00000011b
	mov	r0, a

; increment the r7/r6 pointer
	add	a, r6
	mov	r6, a
	mov	a, r7
	addc	a, #0
	mov	r7, a

; now print the bytes and spaces (r0 has # of bytes to print)
.print_bytes:
	mov	a, #':'
	acall	cout_h
	acall	space_h
	jb	psw.1, .print_mnemonic		; skip bytes if running single-step
	mov	a, r2
	acall	phex_h
	acall	space_h
	cjne	r0, #1, +
	mov	r1, #11
	sjmp	.print_spaces
+	mov	a, r3
	acall	phex_h
	acall	space_h
	cjne	r0, #2, +
	mov	r1, #8
	sjmp	.print_spaces
+	mov	a, r4
	acall	phex_h
	mov	r1, #6
.print_spaces:
	acall	space_h
	djnz	r1, .print_spaces


; prints the mnemonic name and spaces
.print_mnemonic:
	mov	a, r2
	anl	a, #00001000b
	jnz	.pmne_lookup2
.mne_lookup1:
	mov	dptr, #mnot1			; mnot=mnemonic offset table
	mov	a, r2
	rr	a
	anl	a, #01111000b			; grab upper 4 bits
	mov	r0, a				; keep in r0 for a moment
	mov	a, r2
	anl	a, #00000111b			; get lower 3 bits
	orl	a, r0				; combine in upper 4
	movc	a, @a+dptr
	mov	r1, a
	sjmp	.serial_output
.pmne_lookup2:
	mov	dptr, #mnot2			; 16 byte table for r0-r7 instructions
	mov	a, r2
	swap	a
	anl	a, #00001111b
	movc	a, @a+dptr
	mov	r1, a
.serial_output:
	mov	dptr, #mnu_tbl
	mov	r0, #8
	clr	c
.print_loop:
	mov	a, #' '
	jc	.print
	mov	a, r1
	movc	a, @a+dptr
	inc	r1
	mov	c, acc.7
	anl	a, #07FH
.print:
	acall	cout_h
	djnz	r0, .print_loop

; print the operands
	mov	a, #.dasm2 & 255		; (low)
	push	acc
	mov	a, #.dasm2 >> 8			; (high)
	push	acc

.am_lookup0:
	mov	a, r2
	anl	a, #00001000b
	jnz	.am_lookup2

; fetch constants for instructions not using R0-R7
.am_lookup1:
	mov	a, r2
	rr	a
	anl	a, #01111000b			; grab upper 4 bits
	mov	r0, a				; keep in r0 for a moment
	mov	a, r2
	anl	a, #00000111b			; get lower 3 bits
	orl	a, r0				; combine in upper 4
	mov	dptr, #opot1			; opot=operand offset table
	movc	a, @a+dptr
	sjmp	.am_unpack

; fetch constants for R0-R7 instructions
.am_lookup2:
	mov	a, r2
	swap	a
	anl	a, #00001111b
	mov	dptr, #opot2
	movc	a, @a+dptr

.am_unpack:
	anl	a, #11111100b
	rr	a
	rr	a
	dec	a

	mov	dptr, #oprt			; oprt=operand routine table
	rl	a
	add	a, dpl
	mov	dpl, a
	clr	a
	addc	a, dph
	mov	dph, a
	clr	a
	jmp	@a+dptr
.dasm2:
	ajmp	newline_h


oprt:   ajmp	opcd1				; addr11
	ajmp 	opcd2				; A,Rn
	ajmp	opcd3				; A,direct
	ajmp	opcd4				; A,@Ri
	ajmp	opcd5				; A,#data
	ajmp	opcd6				; direct,A
	ajmp	opcd7				; direct,#data
	ajmp	opcd8				; C,bit
	ajmp	opcd9				; C,/bit
	ajmp	opcd10				; A,direct,rel
	ajmp	opcd11				; A,#data,rel
	ajmp	opcd12				; Rn,#data,rel
	ajmp	opcd13				; @Ri,#data,rel
	ajmp	pa				; A
	ajmp	prc				; C
	ajmp	pbit				; bit
	ajmp	pdirect				; direct
	ajmp	p_reg_i				; @Ri
	ajmp	opcd19				; AB
	ajmp	opcd20				; Rn,rel
	ajmp	opcd21				; direct,rel
	ajmp	p_reg_n				; Rn
	ajmp	pdptr				; DPTR
	ajmp	opcd24				; bit,rel
	ajmp	prel				; rel
	ajmp	opcd26				; @A+DPTR
	ajmp	opcd27				; addr16
	ajmp	opcd28				; Rn,A
	ajmp	opcd29				; Rn,direct
	ajmp	opcd30				; Rn,#data
	ajmp	opcd31				; direct,Rn
	ajmp	opcd32				; direct,direct
	ajmp	opcd33				; direct,@Ri
	ajmp	opcd34				; @Ri,A
	ajmp	opcd35				; @Ri,direct
	ajmp	opcd36				; @Ri,#data
	ajmp	opcd37				; bit,C
	ajmp	opcd38				; DPTR,#data16
	ajmp	opcd39				; A,@A+DPTR
	ajmp	opcd40				; A,@A+PC
	ajmp	opcd41				; A,@DPTR
	ajmp	opcd42				; @DPTR,A
	ret					; <nothing>



opcd4:
	; A, @Ri
	acall	pac
p_reg_i:
	mov	a,#'@'
	acall	cout_h
	mov	a,#'R'
	acall	cout_h
	mov	a, r2
	anl	a,#00000001b
	ajmp	phex1_h

opcd3:
	; A, direct
	acall	pac
pdirect:
	mov	a, r3
	jb	acc.7,pdir1
pdir0:
	mov	a, r3
	ajmp	phex_h
pdir1:
	mov	dptr,#sfrmnu
pdir2:
	clr	a
	movc	a,@a+dptr
	inc	dptr
	jz	pdir0
	mov	r0,a
	clr	c
	subb	a, r3
	jnz	pdir3
pstr_h:
	ljmp	pm.pstr

pdir3:
	clr	a
	movc	a,@a+dptr
	inc	dptr
	jnb	acc.7,pdir3
	sjmp	pdir2


opcd9:
	; C, /bit
	acall	prc
	acall	pcomma
	mov	a, #'/'
	acall	cout_h
pbit:
	mov	a, r3
	anl	a,#01111000b
	rl	a
	swap	a
	mov	r0,a
	mov	a, r3
	anl	a,#10000000b
	jz	pbit1
	mov	dptr,#bitptr			; it's a Special Function Reg.
	mov	a,r0
	movc	a,@a+dptr
	mov	dptr,#bitmnu
	addc	a,dpl
	mov	dpl,a
	jnc	pbit0
	inc	dph
pbit0:
	acall	pstr_h
	sjmp	pbit2
pbit1:
	mov	a,r0				; it's between 20h and 2Fh
	add	a,#20h
	acall	phex_h
pbit2:
	mov	a,#'.'
	acall	cout_h
	mov	a, r3
	anl	a,#00000111b
	ajmp	phex1_h


opcd10:
	; A, direct, rel
	acall	pac
	acall	pdirect
opcd10a:
	acall	pcomma
	mov	a, r4
	mov	r3, a
prel:
	mov	a, r3
	add	a, r6
	mov	dpl, a
	mov	a, r3
	jb	acc.7, prel2
	clr	a
	sjmp	prel3
prel2:
	clr	a
	cpl	a
prel3:
	addc	a, r7
	mov	dph, a
	ljmp	pm.phex16


pat:
	; prints the '@' symbol
	mov	a,#'@'
	ajmp	cout_h


pac:
	; print "A,"
	acall	pa


pcomma:
	; prints a comma
	mov	a,#','
	acall	cout_h


pspace:
	; prints a space
	mov	a, #' '
	ajmp	cout_h


plb:
	; prints the '#' symbol
	mov	a,#'#'
	ajmp	cout_h


opcd6:
	; direct,A
	acall	pdirect
	acall	pcomma
pa:
	; prints 'A'
	mov	a,#'A'
	ajmp	cout_h

opcd37:
	; bit, C
	acall	pbit
	acall	pcomma
prc:
	; prints 'C'
	mov	a,#'C'
	ajmp	cout_h

opcd26:
	; @A+DPTR
	acall	pat
	acall	pa
	mov	a,#'+'
	acall	cout_h
pdptr:
	; prints DPTR
	mov	a, #'D'
	acall	cout_h
	mov	a, #'P'
	acall	cout_h
	mov	a, #'T'
	acall	cout_h
	mov	a, #'R'
cout_h:
	ljmp	pm.cout


opcd1:
	mov	a, r7				; addr11
	anl	a, #11111000b
	mov	r0, a
	mov	a, r2
	swap	a
	rr	a
	anl	a, #00000111b
	orl	a, r0
	acall	phex_h
	mov	a, r3
	ajmp	phex_h


opcd2:
	;A, Rn
	acall	pac
p_reg_n:
	mov	a,#'R'
	acall	cout_h
	mov	a, r2
	anl	a,#00000111b
phex1_h:
	ljmp	pm.phex1



opcd5:
	; A, #data
	acall	pa
pdata:
	acall	pcomma
	acall	plb
	mov	a, r3
phex_h:
	ljmp	pm.phex



opcd7:
	; direct, #data
	acall	pdirect
	mov	a, r4
	mov	r3, a
	ajmp	pdata


opcd8:
	; C, bit
	acall	prc
	acall	pcomma
	ajmp	pbit


opcd11:
	; A,#data, rel
	acall	pa
opcd11a:
	acall	pcomma
	acall	plb
	mov	a, r3
	acall	phex_h
	ajmp	opcd10a


opcd12:
	; Rn,#data, rel
	acall	p_reg_n
	ajmp	opcd11a


opcd13:
	; @Ri,#data, rel
	acall	p_reg_i
	ajmp	opcd11a


opcd19:
	; AB
	acall	pa
	mov	a, #'B'
	ajmp	cout_h


opcd20:
	; Rn, rel
	acall	p_reg_n
	acall	pcomma
	ajmp	prel


opcd21:
	; direct, rel
	acall	pdirect
	ajmp	opcd10a


opcd24:
	; bit, rel
	acall	pbit
	ajmp	opcd10a


opcd28:
	; Rn, A
	acall	p_reg_n
	acall	pcomma
	ajmp	pa


opcd29:
	; Rn, direct
	acall	p_reg_n
	acall	pcomma
	ajmp	pdirect


opcd30:
	; Rn, #data
	acall	p_reg_n
	ajmp	pdata


opcd31:
	; direct, Rn
	acall	pdirect
	acall	pcomma
	ajmp	p_reg_n


opcd32:
	; direct, direct
	mov	a, r3
	push	acc
	mov	a, r4
	mov	r3, a
	acall	pdirect
	acall	pcomma
	pop	acc
	mov	r3, a
	ajmp	pdirect


opcd33:
	; direct, @Ri
	acall	pdirect
	acall	pcomma
	ajmp	p_reg_i


opcd34:
	;@Ri, A
	acall	p_reg_i
	acall	pcomma
	ajmp	pa


opcd35:
	;@Ri, direct
	acall	p_reg_i
	acall	pcomma
	ajmp	pdirect


opcd36:
	; @Ri, #data
	acall	p_reg_i
	ajmp	pdata


opcd38:
	; DPTR, #data16
	acall	pdptr
	acall	pcomma
	acall	plb


opcd27:
	; addr16
	mov	a, r3
	acall	phex_h
	mov	a, r4
	ajmp	phex_h


opcd39:
	; A, @A+DPTR
	acall	pac
	acall	pat
	acall	pa
	mov	a,#'+'
	acall	cout_h
	ajmp	pdptr


opcd40:
	; A, @A+PC
	acall	pac
	acall	pat
	acall	pa
	mov	a,#'+'
	acall	cout_h
	mov	a,#'P'
	acall	cout_h
	ajmp	prc


opcd41:
	; A, @DPTR
	acall	pac
	acall	pat
	ajmp	pdptr


opcd42:
	; @DPTR, A
	acall	pat
	acall	pdptr
	acall	pcomma
	ajmp	pa


sfrmnu: db	0E0H,"AC",'C' + 128
	db	081H,'S','P' + 128
	db	082H,"DP",'L' + 128
	db	083H,"DP",'H' + 128
	db	080H,'P','0' + 128
	db	090H,'P','1' + 128
	db	0A0H,'P','2' + 128
	db	0B0H,'P','3' + 128
	db	099H,"SBU",'F' + 128
	db	0CDH,"TH",'2' + 128
	db	0C8H,"T2CO",'N' + 128
	db	0CCH,"TL",'2' + 128
	db	0CBH,"RCAP2",'H' + 128
	db	0CAH,"RCAP2",'L' + 128
	db	08CH,"TH",'0' + 128
	db	08AH,"TL",'0' + 128
	db	08DH,"TH",'1' + 128
	db	08BH,"TL",'1' + 128
sfr1:   db	0F0H,'B' + 128		; 5
sfr2:   db	0D0H,"PS",'W' + 128	; 7
sfr3:   db	0A8H,'I','E' + 128
sfr4:   db	0B8H,'I','P' + 128
sfr5:   db	089H,"TMO",'D' + 128	; 8
sfr6:   db	088H,"TCO",'N' + 128	; 8
sfr7:   db	098H,"SCO",'N' + 128	; 8
sfr8:   db	087H,"PCO",'N' + 128	; 8
	db	0			;just in case


opot2:  db	059H, 059H, 009H, 009H	; inc, dec, add, addc
	db	009H, 009H, 009H, 07AH	; orl, anl, xrl, mov
	db	07EH, 009H, 076H, 033H	; mov, subb, mov, cjne
	db	009H, 052H, 009H, 071H	; xch, djnz, mov, mov

bitptr: db	000H, 002H, 006H, 008H, 00CH, 00EH, 010H, 012H
	db	014H, 016H, 01BH, 01EH, 020H, 023H, 024H, 025H


; some stuff used by single step... it's here to fill up some of
; the unused space from the end of the disassembler code and the
; beginning of the single-step header (which must begin on a 256
; byte page boundry)


wr_check:
	; write to memory and check that it worked.
	; acc=0 if it worked, nonzero if it didn't write
	mov	r0, a				; keep a copy of the data in r0
	movx	@dptr, a
	clr	a
	movc	a, @a+dptr
	clr	c
	subb	a, r0
	ret

chardly:
	; delay for approx 1 character transmit time
	mov	r1, #80
chdly2:
	mov	a, th1
	cpl	a
	inc	a
	mov	r0, a
	djnz	r0, $
	djnz	r1, chdly2
	ret

prcolon:
	acall	phex_h
	mov	a, #':'
	ajmp	cout_h

phexsp:
	acall	phex_h
space_h:
	mov	a, #' '
	ajmp	cout_h


;SINGLE
;---------------------------------------------------------;
;                                                         ;
;                 single step command                     ;
;                                                         ;
;---------------------------------------------------------;

	org	locat + 0400H
	db	0A5H,0E5H,0E0H,0A5H		;signiture
	db	254,step_key,0,0		;id (254=user installed command)
	db	0,0,0,0				;prompt code vector
	db	0,0,0,0				;reserved
	db	0,0,0,0				;reserved
	db	0,0,0,0				;reserved
	db	0,0,0,0				;user defined
	db	255,255,255,255			;length and checksum (255=unused)
	db	"Single-Step",0
	org	locat + 0440H			;executable code begins here



ssrun:
	; first check to make sure they connect int1 low
	jnb	p3.3, ssrun2
	mov	dptr, #sserr1			; give error msg if int1 not grounded
pcstr_h:
	ljmp	pm.pcstr

ssrun2:
	; make sure there's a ljmp at the int1 vector location
	mov	dptr, #00013H
	clr	a
	movc	a, @a+dptr
	add	a, #254
	jz	ssrun3
	mov	dptr, #sserr2			; give error that paulmon2 was not found.
	ajmp	pcstr_h
ssrun3:
	; now write an ljmp to "step" in the ram and check it.
	inc	dptr
	movc	a, @a+dptr
	mov	r0, a
	clr	a
	inc	dptr
	movc	a, @a+dptr
	mov	dpl, a
	mov	dph, r0				; now data pointer points to int1 target
	mov	a, #2
	acall	wr_check
	jnz	ssrun4
	inc	dptr
	mov	a, #(step >> 8)
	acall	wr_check
	jnz	ssrun4
	inc	dptr
	mov	a, #(step & 255)
	acall	wr_check
	jz	ssrun5
ssrun4:
	mov	r0, dpl
	mov	r1, dph
	mov	dptr, #sserr3			; error: couldn't write to memory @xxxx
	acall	pcstr_h
	mov	a, r1
	acall	phex_h
	mov	a, r0
	acall	phex_h
	ajmp	newline_h
ssrun5:
	mov	a, ip				; set to high priority interrupt
	anl	a, #00000100b
	mov	ip, a
	; let's not beat around the bush (like paulmon1), all
	; we need to know is where to jump into memory.
	mov	dptr, #prompt8
	acall	pcstr_h
	mov	a, r7
	acall	phex_h
	mov	a, r6
	acall	phex_h
	mov     dptr,#prompt4
	acall   pcstr_h
	lcall   pm.ghex16			; ask for the jump location
	jb	psw.5, ssrun7
	jnc     ssrun6
	mov     dptr,#abort
	acall   pcstr_h
	ajmp    newline_h
ssrun6:
	mov	r6, dpl				; where we'll begin executing
	mov	r7, dph
ssrun7:
	clr	tcon.2				; need low-level triggered int1
	mov     dptr,#ssmsg			; tell 'em we're starting
	acall	pcstr_h
	mov	dptr,#ssnames
	acall	pstr_h
	clr	a
	mov	sp, #8				; just like after a reset
	push	acc				; unlike a 8051 start-up, push return addr
	push	acc				; of 0000, just in case they end w/ ret
	mov	dpl, r6				; load the program's address into dptr
	mov	dph, r7
	mov	psw, a				; and clear everything to zero
	mov	r0, a
	mov	r1, a
	mov	r2, a
	mov	r3, a
	mov	r4, a
	mov	r5, a
	mov	r6, a
	mov	r7, a
	mov	b, a
	mov	lastpc, #ssstart & 255
	mov	(lastpc + 1), #ssstart >> 8
	setb	ie.2
	setb	ea				; turn on the interrupt
ssstart:
	jmp	@a+dptr


done:
	acall	chardly
	pop	acc
	mov	r1, a
	pop	acc
	mov	r0, a
	pop	dpl
	pop	dph
	pop	psw
	pop	acc
	reti

step:
	; this is the single step interrupt service code...
	push	acc
	push	psw				; Stack Contents: (in this order)
	push	dph				; PC_L PC_H ACC PSW DPH DPL R0 R1
	push	dpl
	mov	a, r0
	push	acc
	mov	a, r1
	push	acc
	; in case the previous instruction was "clr ti", we
	; must wait for a character transmit time "in case it
	; was a move to SBUF) and then set ti so that our cout
	; doesn't hang when we transmit the first character!
	acall	chardly
	setb	ti

	; now print out a line that looks like this:
	; ACC B C DPTR  R0 R1 R2 R3 R4 R5 R6 R7  SP    PC  Instruction
	; 00 00 0 3F00  00:00:00:00:00:00:00:00  00 - 0000: LJMP    0825

	acall	space_h
	acall	space_h
	mov	a, sp
	add	a, #251
	mov	r0, a				; r0 points to user's acc on stack
	mov	a, @r0
	acall	phexsp				; print acc
	mov	a, b
	acall	phexsp				; print b register
	inc	r0
	mov	a, @r0
	rl	a
	anl	a, #1
	acall	phex1_h				; print carry bit
	acall	space_h
	inc	r0
	mov	a, @r0
	acall	phex_h				; print dptr (msb)
	inc	r0
	mov	a, @r0
	acall	phexsp				; print dptr (lsb)
	acall	space_h
	inc	r0
	mov	a, @r0
	acall	prcolon				; print r0
	inc	r0
	mov	a, @r0
	acall	prcolon				; print r1
	mov	a, r2
	acall	prcolon				; print r2
	mov	a, r3
	acall	prcolon				; print r3
	mov	a, r4
	acall	prcolon				; print r4
	mov	a, r5
	acall	prcolon				; print r5
	mov	a, r6
	acall	prcolon				; print r6
	mov	a, r7
	acall	phexsp				; print r7
	acall	space_h
	mov	a, r0
	add	a, #248
	acall	phexsp				; print stack pointer
	acall	space_h
	acall	space_h
	; now the trick is to disassemble the instruction... this isn't
	; easy, since the user wants to see the last instruction that
	; just executed, but program counter on the stack points to the
	; next instruction to be executed.  The dirty trick is to grab
	; the program counter from last time where we stashed it in some
	; memory that hopefully the user's program hasn't overwritten.
	mov	a, lastpc
	mov	lastpc, r6
	mov	r6, a
	mov	a, (lastpc + 1)
	mov	(lastpc + 1), r7
	mov	r7, a
	mov	a, r2
	push	acc
	mov	a, r3
	push	acc
	mov	a, r4
	push	acc
	setb	psw.1				; tell it to use a compact format
	; the disassembler uses quite a bit of stack space... if the
	; user didn't leave enough room for the stack to grow with
	; all this overhead, it will likely crash somewhere in the
	; disassembler... oh well, not much I can do about it.  The
	; worst case stack usage for disasm is 9 bytes.  We just
	; pushed 5 and 6 at the beginning of step.  With the two
	; bytes for the interrupt, a total of 22 bytes of free stack
	; space must be available to use the single-step feature.
	acall	disasm
	pop	acc
	mov	r4, a
	pop	acc
	mov	r3, a
	pop	acc
	mov	r2, a
	mov	r7, (lastpc + 1)
	mov	r6, lastpc
	; now grab the user's PC value to keep it for next time
	mov	a, sp
	add	a, #249
	mov	r0, a				; r0 points to user's acc on stack
	mov	a, @r0
	mov	lastpc, a
	inc	r0
	mov	a, @r0
	mov	(lastpc + 1), a

; SINGLE STEP

step1:
	lcall	pm.cin_filter
	lcall	pm.upper
step2:
	cjne	a, #13, step7
	ajmp	done
step7:
	cjne	a, #' ', step8			; check space
	ajmp	done
step8:
	cjne	a,#'?',step10			; check '?'
	mov	dptr,#help5txt
	acall	pcstr_h
	ajmp	step1
step10:
	cjne	a,#'Q',step11			; check 'Q'=quit and run normal
	mov	dptr, #squit
	acall	pstr_h
	clr	ie.2
	acall	chardly
	mov	8, #0				; force return to 0000
	mov	9, #0
	mov	sp, #9
	reti
step11:
	cjne	a,#'H',step12			; check 'H'=hex dump internal ram
	ajmp	ssdmp
step12:
	cjne	a,#'R',step13			; check 'R'=print out registers
	ajmp	ssreg
step13:
	cjne	a,#'S',step14			; check 'S'=skip next inst
	ajmp	ssskip
step14:
	cjne	a,#'A',step15			; check 'A'=change acc value
	ajmp	sschacc
step15:
	cjne	a,#'.',step20
	mov	dptr, #ssnames
	acall	pstr_h
	ajmp	step1

step20:
	ajmp	step1


pequal:
	; prints '='
	mov	a,#'='
	ajmp	cout_h

ssdmp:
	mov	dptr, #ssdmps1
	acall	pcstr_h
	clr	a
	acall	prcolon
	acall	space_h
	mov	r0, sp
	dec	r0
	mov	a, @r0
	acall	phexsp
	inc	r0
	mov	a, @r0
	acall	phex_h
	mov	r0, #2
	mov	r1, #14
	ajmp	ssdmp2
ssdmp1:
	mov	a, r0
	acall	prcolon
	mov	r1, #16
ssdmp2:
	acall	space_h
	mov	a, @r0
	acall	phex_h
	inc	r0
	djnz	r1, ssdmp2
	acall	newline_h
	cjne	r0, #080H, ssdmp1
	acall	newline_h
	ajmp	step1


ssreg:
	mov	dptr, #sfr2+1
	acall	pstr_h
	acall	pequal
	mov	a, sp
	add	a, #252
	mov	r0, a
	mov	a, @r0
	acall	phexsp				; print psw
	mov	dptr,#sfr3+1
	mov	r0, 0A8H
	acall	psfr				; print ie
	mov	dptr,#sfr4+1
	mov	r0, 0B8H
	acall	psfr				; print ip
	mov	dptr,#sfr5+1
	mov	r0, 089H
	acall	psfr				; print tmod
	mov	dptr,#sfr6+1
	mov	r0, 088H
	acall	psfr				; print tcon
	mov	dptr,#sfr7+1
	mov	r0, 098H
	acall	psfr				; print smod
	mov	dptr,#sfr8+1
	mov	r0, 087H
	acall	psfr				; print pcon
	mov	a, #'T'
	acall	cout_h
	mov	a, #'0'
	acall	cout_h
	acall	pequal
	mov	a, 8Ch
	acall	phex_h				; print Timer 0
	mov	a, 8Ah
	acall	phex_h
	acall	space_h
	mov	a, #'T'
	acall	cout_h
	mov	a, #'1'
	acall	cout_h
	acall	pequal
	mov	a, 8Dh				; print Timer 1
	acall	phex_h
	mov	a, 8Bh
	acall	phex_h
	acall	newline_h
	ajmp	step1

psfr:
	acall	pstr_h
	acall	pequal
	mov	a, r0
	ajmp	phexsp



;skip the next instruction
ssskip:
	mov	r0, #23
ssskip2:
	acall	space_h
	djnz	r0, ssskip2
	mov	dptr,#sskip1
	acall	pcstr_h
	mov	a, sp
	add	a, #249
	mov	r0, a				; set r0 to point to pc on stack
	mov	a, @r0
	mov	lastpc, r6			; keep r6/r7 safe in lastpc
	mov	r6, a				; put user's pc into r6/r7
	inc	r0
	mov	a, @r0
	mov	(lastpc + 1), r7
	mov	r7, a
	mov	a, r2
	push	acc
	mov	a, r3
	push	acc
	mov	a, r4
	push	acc
	setb	psw.1				; tell it to use a compact format
	acall	disasm				; run disasm to show 'em what was skipped
	pop	acc
	mov	r4, a
	pop	acc
	mov	r3, a
	pop	acc
	mov	r2, a
	mov	a, sp
	add	a, #249
	mov	r0, a				; set r0 to point to pc on stack
	mov	a, r6
	mov	r6, lastpc			; restore r6/r7
	mov	lastpc, a			; update lastpc with next inst addr
	mov	@r0, a				; also update user's pc!!
	inc	r0
	mov	a, r7
	mov	r7, (lastpc + 1)
	mov	(lastpc + 1), a
	mov	@r0, a
	ajmp	step1

sschacc:
	mov	a, sp
	add	a, #251
	mov	r0, a				; r0 points to acc on stack
	mov	dptr, #chaccs1
	acall	pstr_h
	lcall	pm.ghex
	jc	chacc2
	jb	psw.5, chacc2
	mov	@r0, a
	acall	newline_h
	ajmp	step1
chacc2:
	mov	dptr, #abort
	acall	pcstr_h
	ajmp	step1




; stuff some of the disassembler tables, strings, etc since we have a
; bit of space before the beginning of the editor command code


; opcode offset table (gives #bytes for the instruction
; and the number of the routine to print the operands)

opot1:  db	0ADH, 006H, 06FH, 039H, 039H, 046H, 049H, 049H ;0
	db	063H, 006H, 06FH, 039H, 039H, 046H, 049H, 049H ;1
	db	063H, 006H, 0ADH, 039H, 016H, 00EH, 011H, 011H ;2
	db	063H, 006H, 0ADH, 039H, 016H, 00EH, 011H, 011H ;3
	db	066H, 006H, 01AH, 01FH, 016H, 00EH, 011H, 011H ;4
	db	066H, 006H, 01AH, 01FH, 016H, 00EH, 011H, 011H ;5
	db	066H, 006H, 01AH, 01FH, 016H, 00EH, 011H, 011H ;6
	db	066H, 006H, 022H, 069H, 016H, 01FH, 092H, 092H ;7
	db	066H, 006H, 022H, 0A1H, 04DH, 083H, 086H, 086H ;8
	db	09BH, 006H, 096H, 09DH, 016H, 00EH, 011H, 011H ;9
	db	026H, 006H, 022H, 05DH, 04DH, 0ADH, 08EH, 08EH ;A
	db	026H, 006H, 042H, 03DH, 02FH, 02BH, 037H, 037H ;B
	db	046H, 006H, 042H, 03DH, 039H, 00EH, 011H, 011H ;C
	db	046H, 006H, 042H, 03DH, 039H, 057H, 011H, 011H ;D
	db	0A5H, 006H, 011H, 011H, 039H, 00EH, 011H, 011H ;E
	db	0A9H, 006H, 089H, 089H, 039H, 01AH, 089H, 089H ;F

mnot1:
	; mnemonic offset table (gives offset into above table)

	db	05AH, 00EH, 048H, 073H  ; nop, ajmp, ljmp, rr
	db	02BH, 02BH, 02BH, 02BH  ; inc, inc, inc, inc
	db	030H, 000H, 043H, 075H  ; jbc, acall, lcall rrc
	db	021H, 021H, 021H, 021H  ;

	db	02EH, 00EH, 067H, 06EH  ; etc...
	db	006H, 006H, 006H, 006H  ;
	db	038H, 000H, 06AH, 070H  ;
	db	00AH, 00AH, 00AH, 00AH  ;

	db	033H, 00EH, 05DH, 05DH  ;
	db	05DH, 05DH, 05DH, 05DH  ;
	db	03BH, 000H, 012H, 012H  ;
	db	012H, 012H, 012H, 012H  ;

	db	041H, 00EH, 08FH, 08FH  ;
	db	08FH, 08FH, 08FH, 08FH  ;
	db	03EH, 000H, 05DH, 035H  ;
	db	04CH, 04CH, 04CH, 04CH  ;

	db	07CH, 00EH, 012H, 04FH  ;
	db	024H, 04CH, 04CH, 04CH  ;
	db	04CH, 000H, 04CH, 04FH  ;
	db	080H, 080H, 080H, 080H  ;

	db	05DH, 00EH, 04CH, 02BH  ;
	db	057H, 092H, 04CH, 04CH  ;
	db	012H, 000H, 01CH, 01CH  ;
	db	015H, 015H, 015H, 015H  ;

	db	063H, 00EH, 019H, 019H  ;
	db	084H, 088H, 088H, 088H  ;
	db	060H, 000H, 078H, 078H  ;
	db	01FH, 027H, 08BH, 08BH  ;

	db	053H, 00EH, 053H, 053H  ;
	db	019H, 04CH, 04CH, 04CH  ;
	db	053H, 000H, 053H, 053H  ;
	db	01CH, 04CH, 04CH, 04CH  ;


mnot2:  db	02BH, 021H, 006H, 00AH  ; inc, dec, add, addc
	db	05DH, 012H, 08FH, 04CH  ; orl, anl, xlr, mov
	db	04CH, 080H, 04CH, 015H  ; mov, subb, mov, cjne
	db	088H, 027H, 04CH, 04CH  ; xch, djnz, mov, mov


;---------------------------------------------------------;
;                                                         ;
;                  External Memory Editor                 ;
;                                                         ;
;---------------------------------------------------------;

; register usage:
; R4,    Flags:
;         bit0: 0=display CODE memory, 1=display DATA memory
;         bit1: 0=editing disabled, 1=editing enabled
;         bit2: 0=editing in hex, 1=editing in ascii
;         bit3: 0=normal, 1=in middle of hex entry (value in r5)
; R6/R7, current memory location
;

	org	locat + 0800H
	db	0A5H,0E5H,0E0H,0A5H		; signiture
	db	254,vtedit_key,0,0		; id (254=user installed command)
	db	0,0,0,0				; prompt code vector
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; reserved
	db	0,0,0,0				; user defined
	db	255,255,255,255			; length and checksum (255=unused)
	db	"Memory Editor (VT100)",0

	org	locat + 00840H			; executable code begins here


	mov	r4, #0
	acall	redraw
main:
	mov	a, r4
	clr	acc.3
	mov	r4, a
main2:
	lcall	pm.cin_filter
	acall	input_ck_2nd
cmd1:
	cjne	a, #27, cmd2			; quit
	ajmp	quit
cmd2:
	cjne	a, #11, cmd3			; up
	ajmp	cmd_up
cmd3:
	cjne	a, #10, cmd4			; down
	ajmp	cmd_down
cmd4:
	cjne	a, #8, cmd5			; left
	ajmp	cmd_left
cmd5:
	cjne	a, #21, cmd6			; right
	ajmp	cmd_right
cmd6:
	cjne	a, #12, cmd7			; redraw
	acall	redraw
	ajmp	main
cmd7:
	cjne	a, #17, cmd8			; quit
	ajmp	quit
cmd8:
	cjne	a, #3, cmd9			; code memory
	mov	a, r4
	anl	a, #11111110b
	mov	r4, a
	acall	cursor_home
	mov	dptr, #str_code
	acall	pstr_hh
	acall	redraw_data
	ajmp	main
cmd9:
	cjne	a, #4, cmd10			; data memory
	mov	a, r4
	orl	a, #00000001b
	mov	r4, a
	acall	cursor_home
	mov	dptr, #str_data
	acall	pstr_hh
	acall	redraw_data
	ajmp	main
cmd10:
	cjne	a, #7, cmd11			; goto memory loc
	ajmp	cmd_goto
cmd11:
	cjne	a, #5, cmd12			; toggle editing
	ajmp	cmd_edit
cmd12:
	cjne	a, #6, cmd13			; fill memory
	ajmp	cmd_fill
cmd13:
	cjne	a, #1, cmd14			; edit in ascii
	mov	a, r4
	jnb	acc.1, main
	setb	acc.2
	mov	r4, a
	acall	erase_commands
	acall	print_commands
	acall	redraw_cursor
	ajmp	main
cmd14:
	cjne	a, #24, cmd15			; edit in hex
	mov	a, r4
	jnb	acc.1, main
	clr	acc.2
	mov	r4, a
	acall	erase_commands
	acall	print_commands
	acall	redraw_cursor
	ajmp	main
cmd15:
	cjne	a, #25, cmd16			; page up
	ajmp	cmd_pgup
cmd16:
	cjne	a, #26, cmd17			; page down
	ajmp	cmd_pgdown
cmd17:


cmd_data:
	; the input character wasn't any particular command, so
	; maybe it's some input data being typed for edit mode
	mov	b, a				; keep a copy of user data in b
	mov	a, r4
	jb	acc.1, cmd_data2
cmd_abort:
	ajmp	main				; ignore if not in edit mode
cmd_data2:
	jnb	acc.2, input_hex
input_ascii:
	mov	a, b
	acall	ascii_only
	cjne	a, b, cmd_abort			; check that input is an ascii char
	mov	dph, r7
	mov	dpl, r6
	lcall	pm.smart_wr			; write the char to memory
	ajmp	cmd_right

input_hex:
	mov	a, b
	lcall	pm.upper
	lcall	pm.asc2hex
	jc	cmd_abort			; ignore if not hex
	mov	r0, a				; keep hex value of input in r0
	mov     dph, r7				; load dptr with address
	mov     dpl, r6
	mov	a, r4
	jb	acc.3, input_hex_2nd
input_hex_1st:
	mov	a, r0
	mov	r5, a
	mov	a, r4
	setb	acc.3				; remember that we're waiting for 2nd char
	mov	r4, a
	acall	redraw_cursor
	ajmp	main2
input_hex_2nd:
	mov	a, r5				; get data from memory
	swap	a				; shift nibbles
	anl	a, #11110000b			; just in case
	add	a, r0				; add in this input to lower part
	lcall	pm.smart_wr			; write back to memory
	mov	a, r4
	clr	acc.3
	mov	r4, a
	ajmp	cmd_right

input_ck_2nd:
	; the main input routine will always call here when a new
	; byte is entered... so we can do something special if we
	; were waiting for the second character and it is not a
	; legal hex character
	push	acc
	mov	a, r4
	jb	acc.1, inck2d
	; if editing is disabled, don't do anything
	clr	acc.3
inck2b:
	mov	r4, a
inck2c:
	pop	acc
	ret
inck2d:
	jnb	acc.3, inck2b
	; if we get here, we were actually waiting for the 2nd char
	pop	acc
	push	acc
	lcall	pm.upper
	lcall	pm.asc2hex
	jnc	inck2c				; proceed normally if it is valid
	; if we get here, we did not get a hex legal char
	pop	acc
	push	acc
	cjne	a, #esc_char, inck2e
	mov	a, r4
	clr	acc.3
	mov	r4, a
	acall	redraw_cursor
	pop	acc
	pop	acc				; don't return and do the quit cmd
	pop	acc				; just quit this entry and wait for next cmd
	ajmp	main
inck2e:
	mov	dph, r7				; load dptr with address
	mov	dpl, r6
	mov	a, r5
	lcall	pm.smart_wr			; write to memory
	mov	a, r4
	clr	acc.3
	mov	r4, a
	acall	redraw_cursor
	sjmp	inck2c

; R4,    Flags:
;         bit0: 0=display CODE memory, 1=display DATA memory
;         bit1: 0=editing disabled, 1=editing enabled
;         bit2: 0=editing in hex, 1=editing in ascii
;         bit3: 0=normal, 1=in middle of hex entry (value in r5)


cmd_fill:
	mov	a, r4
	anl	a, #00000010b
	jnz	cmd_fill_ok
	ajmp	main				; don't allow if not in editing mode
cmd_fill_ok:
	acall	erase_commands
	mov	a, r4
	push	acc
	mov	dptr, #fill_prompt1
	acall	pcstr_hh
	lcall	pm.ghex16
	jc	cmd_fill_abort
	jb	psw.5, cmd_fill_abort
	mov	r0, dpl
	mov	r1, dph
	mov	dptr, #fill_prompt2
	acall	pstr_hh
	lcall	pm.ghex16
	jc	cmd_fill_abort
	jb	psw.5, cmd_fill_abort
	mov	r4, dpl
	mov	r5, dph
	mov	dptr, #fill_prompt3
	acall	pcstr_hh
	lcall	pm.ghex
	jc	cmd_fill_abort
	jb	psw.5, cmd_fill_abort
	mov	r2, a
	mov	a, r4
	mov	r6, a
	mov	a, r5
	mov	r7, a
	pop	acc
	mov	r4, a
	mov	dpl, r0
	mov	dph, r1
	; now r4 is restored to its normal value, dptr holds the
	; first location to fill, and r6/r7 holds the last location to
	; fill, and r2 has the fill value.
cmd_fill_loop:
	mov	a, r2
	lcall	pm.smart_wr
	mov	a, r6
	cjne	a, dpl, cmd_fill_next
	mov	a, r7
	cjne	a, dph, cmd_fill_next
	; when we get here, we're done!
	acall	erase_commands
	acall	print_commands
	acall	redraw_data
	ajmp	main
cmd_fill_next:
	inc	dptr
	sjmp	cmd_fill_loop

cmd_fill_abort:
	pop	acc
	mov	r4, a
	acall	erase_commands
	acall	print_commands
	acall	redraw_cursor
	ajmp	main

cmd_edit:
	acall	erase_commands
	mov	a, r4
	xrl	a, #00000010b
	mov	r4, a
	acall	print_commands
	acall	redraw_cursor
	ajmp	main

cmd_goto:
	acall	erase_commands
	mov	dptr, #goto_prompt
	acall	pcstr_hh
	mov	a, r4
	push	acc
	lcall	pm.ghex16
	pop	acc
	mov	r4, a
	jc	cmdgt_abort
	jb	psw.5, cmdgt_abort
	mov	r6, dpl
	mov	r7, dph
	acall	cursor_home
	mov	a, #20
	acall	cursor_down
	acall	print_commands
	acall	redraw_data
	ajmp	main
cmdgt_abort:
	acall	cursor_home
	mov	a, #20
	acall	cursor_down
	acall	print_commands
	acall	redraw_cursor
	ajmp	main

cmd_up:
	acall	blank_it
	mov	a, r6
	clr	c
	subb	a, #16
	mov	r6, a
	mov	a, r7
	subb	a, #0
	mov	r7, a
	mov	a, r6
	cpl	a
	anl	a, #11110000b
	jz	cmd_up_scroll
	mov	a, #1
	acall	cursor_up
	acall	invert_it
	ajmp	main
cmd_up_scroll:
	acall	redraw_data
	ajmp	main

cmd_pgup:
	dec	r7
	acall	redraw_data
	ajmp	main

cmd_pgdown:
	inc	r7
	acall	redraw_data
	ajmp	main

cmd_down:
	acall	blank_it
	mov	a, r6
	add	a, #16
	mov	r6, a
	mov	a, r7
	addc	a, #0
	mov	r7, a
	mov	a, r6
	anl	a, #11110000b
	jz	cmd_down_scroll
	mov	a, #1
	acall	cursor_down
	acall	invert_it
	ajmp	main
cmd_down_scroll:
	acall	redraw_data
	ajmp	main


cmd_left:
	acall	blank_it
	mov	a, #3
	acall	cursor_left
	mov	a, r6
	clr	c
	subb	a, #1
	mov	r6, a
	mov	a, r7
	subb	a, #0
	mov	r7, a
	mov	a, r6
	orl	a, #11110000b
	cpl	a
	jz	cmdlf2
	acall	invert_it
	ajmp	main
cmdlf2:
	mov	a, r6
	cpl	a
	anl	a, #11110000b
	jz	cmd_left_scroll
	mov	a, #48
	acall	cursor_right
	mov	a, #1
	acall	cursor_up
	acall	invert_it
	ajmp	main
cmd_left_scroll:
	acall	redraw_data
	ajmp	main


cmd_right:
	acall	blank_it
	mov	a, #3
	acall	cursor_right
	mov	dpl, r6
	mov	dph, r7
	inc	dptr
	mov	r6, dpl
	mov	r7, dph
	mov	a, r6
	anl	a, #00001111b
	jz	cmdrt2
	acall	invert_it
	ajmp	main

cmdrt2:
	mov	a, r6
	anl	a, #11110000b
	jz	cmd_right_scroll
	mov	a, #48
	acall	cursor_left
	mov	a, #1
	acall	cursor_down
	acall	invert_it
	ajmp	main
cmd_right_scroll:
	acall	redraw_data
	ajmp	main


space:
	mov	a, #' '
	ajmp	cout_hh



; register usage:
; R4,    Flags:
;         bit0: 0=display CODE memory, 1=display DATA memory
;         bit1: 0=editing disabled, 1=editing enabled
;         bit2: 0=editing in hex, 1=editing in ascii
;	  bit3: 0=normal, 1=in middle of hex entry (value in r5)
; R6/R7, current memory location
;


redraw:
	mov	dptr, #str_cl			; clear screen
	acall	pstr_hh
	acall	print_title_line
	acall	newline_hh
	acall	print_addr_line
	acall	newline_hh
	acall	print_dash_line
	acall   newline_hh
	mov	a, #16
	acall	cursor_down
	acall   print_dash_line
	acall   newline_hh
	acall   print_commands
redraw_data:
	acall	cursor_home
	mov	a, #2
	acall	cursor_down
	; compute first byte address to display on the screen
	mov	dpl, #0
	mov	dph, r7
	; now display the data
	mov	r0, #16
rd2:
	acall	newline_hh
	lcall	pm.phex16
	mov	a, #':'
	acall	cout_hh
	mov	r2, dpl
	mov	r3, dph
rd3:
	acall	space
	acall	read_dptr
	acall	phex_hh
	inc	dptr
	mov	a, dpl
	anl	a, #00001111b
	jnz	rd3
	mov	dpl, r2
	mov	dph, r3
	acall	space
	acall	space
	acall	space
rd4:	acall	read_dptr
	acall	ascii_only
	acall	cout_hh
	inc	dptr
	mov	a, dpl
	anl	a, #00001111b
	jnz	rd4
	djnz	r0, rd2

redraw_cursor:
	acall	cursor_home
	mov	a, r6
	swap	a
	anl	a, #00001111b
	add	a, #3
	acall	cursor_down
	; make the ascii character inverse
	mov	a, r6
	anl	a, #00001111b
	add	a, #56
	acall	cursor_right
	acall	inverse_on
	acall	read_r6r7
	acall	ascii_only
	acall	cout_hh
	acall	inverse_off

	; now make the hex value inverse
	mov	a, r6
	anl	a, #00001111b
	rl	a
	cpl	a
	add	a, #52
	acall	cursor_left
	acall	inverse_on
	acall	read_r6r7
	acall	phex_hh
	ajmp	inverse_off

blank_it:
	mov	a, r6
	anl	a, #00001111b
	rl	a
	cpl	a
	add	a, #49
	acall	cursor_right
	acall	read_r6r7
	acall	ascii_only
	acall	cout_hh
	mov	a, r6
	anl	a, #00001111b
	rl	a
	cpl	a
	add	a, #52
	acall	cursor_left
	acall	read_r6r7
	ajmp	phex_hh

invert_it:
	mov	a, r6
	anl	a, #00001111b
	rl	a
	cpl	a
	add	a, #49
	acall	cursor_right
	acall	read_r6r7
	acall	ascii_only
	acall	inverse_on
	acall	cout_hh
	acall	inverse_off
	mov	a, r6
	anl	a, #00001111b
	rl	a
	cpl	a
	add	a, #52
	acall	cursor_left
	acall	read_r6r7
	acall	inverse_on
	acall	phex_hh
	ajmp	inverse_off






quit:	mov	a, r6
	anl	a, #11110000b
	swap	a
	cpl	a
	add	a, #19
	acall	cursor_down
	ajmp	newline_hh


ascii_only:
	anl	a, #01111111b			; avoid unprintable characters
	cjne	a, #127, aonly2
	mov	a, #' '
	ret
aonly2:
	clr	c
	subb	a, #32
	jnc	aonly3				; avoid control characters
	mov	a, #(' ' - 32)
aonly3:
	add	a, #32
	ret



read_dptr:
	mov	a, r4
	jb	acc.0, rddptr2
	clr	a
	movc	a, @a+dptr
	ret
rddptr2:
	movx	a, @dptr
	ret

read_r6r7:
	push	dph
	push	dpl
	mov	dph, r7
	mov	dpl, r6
	mov	a, r4
	jb	acc.3, rdr6r7d
	jb	acc.0, rdr6r7b
	clr	a
	movc	a, @a+dptr
	sjmp	rdr6r7c
rdr6r7b:
	movx	a, @dptr
rdr6r7c:
	pop	dpl
	pop	dph
	ret
rdr6r7d:
	mov	a, r5
	sjmp	rdr6r7c


cursor_home:
	acall	term_esc
	mov	a, #'H'
	ajmp	cout_hh

cursor_down:
	; acc is # of lines to move down
	acall	term_esc
	acall	pint_hh
	mov	a, #'B'
	ajmp	cout_hh

cursor_up:
	; acc is # of lines to move up
	acall	term_esc
	acall	pint_hh
	mov	a, #'A'
	ajmp	cout_hh

cursor_left:
	; acc is # of characters to move left
	acall	term_esc
	acall	pint_hh
	mov	a, #'D'
	ajmp	cout_hh

cursor_right:
	; acc is # of characters to move right
	acall	term_esc
	acall	pint_hh
	mov	a, #'C'
	ajmp	cout_hh

inverse_on:
	mov	dptr, #str_so
	ajmp	pstr_hh


inverse_off:
	mov	dptr, #str_se
	ajmp	pstr_hh

term_esc:
	push	acc
	mov	a, #esc_char
	acall	cout_hh
	mov	a, #'['
	acall	cout_hh
	pop	acc
	ret

print_addr_line:
	mov	dptr, #str_addr
	acall	pstr_hh
	mov	r0, #0
paddrl:
	acall	space
	mov	a, #'+'
	acall	cout_hh
	mov	a, r0
	lcall	pm.phex1
	inc	r0
	cjne	r0, #16, paddrl
	mov	dptr, #str_ascii_equiv
	ajmp	pstr_hh


print_dash_line:
	mov	r0, #72
pdashl:
	mov	a, #'-'
	acall	cout_hh
	djnz	r0, pdashl
	ret

print_title_line:
	mov	a, r4
	jb	acc.0, ptitle2
	mov	dptr, #str_code
	sjmp	ptitle3
ptitle2:
	mov	dptr, #str_data
ptitle3:
	acall	pstr_hh
	mov	r0, #8
ptitlel:
	acall	space
	djnz	r0, ptitlel
	mov	dptr, #str_title
	ajmp	pcstr_hh

erase_commands:
	acall   cursor_home
	mov     a, #20
	acall   cursor_down
	mov     r2, #72
ercmd2:
	acall   space
	djnz    r2, ercmd2
	mov	a, #72
	ajmp	cursor_left


; R4,    Flags:
;         bit0: 0=display CODE memory, 1=display DATA memory
;         bit1: 0=editing disabled, 1=editing enabled
;         bit2: 0=editing in hex, 1=editing in ascii
;         bit3: 0=normal, 1=in middle of hex entry (value in r5)

print_commands:
	mov	a, r4
	jnb	acc.1, pcmd_no_edit
	mov	dptr, #str_cmd3
	jb	acc.2, pcmd_ascii
	mov	dptr, #str_cmd4
pcmd_ascii:
	acall	pstr_hh
	mov	dptr, #str_cmd5
	acall	pstr_hh
	sjmp	pcmd_finish
pcmd_no_edit:
	mov	dptr, #str_cmd2
	acall	pstr_hh
pcmd_finish:
	mov	dptr, #str_cmd1
	ajmp	pstr_hh


; Strings

fill_prompt1:	; "Fill Memory; First: "
	db	"Fill", 31, c_memory,"; First: ",0

fill_prompt2:
	db	"  Last: ",0

fill_prompt3:	; " with: "
	db	" ", c_with, ": ", 0

goto_prompt:	; "Memory Location: "
	db	31, c_memory, 31, c_location, ": ", 0


str_so:
	db	esc_char, "[0;7m", 0

str_se:
	db	esc_char, "[0m", 0


str_cmd1:
	db	"  ^G=Goto  ^C=Code  ^D=Data  ^L=Redraw  ^Q=Quit", 0
str_cmd2:
	db	"^E-Edit",0
str_cmd3:
	db	"^A=", esc_char, "[0;7m", "ASCII", esc_char, "[0m", "  ^X=Hex", 0
str_cmd4:
	db	"^A=ASCII  ^X=", esc_char, "[0;7m", "Hex", esc_char, "[0m", 0
str_cmd5:
	db	"  ^F=Fill",0


str_cl:
	db	esc_char, "[H", esc_char, "[2J", 0

str_addr:
	db	"ADDR:",0
str_ascii_equiv:
	db	"   ASCII EQUIVALENT",0

str_title:	; "8051 External memory editor, Paul Stoffregen, 1996"
	db	"8051", 31, c_external, c_memory, c_edit, "or,"
	db	31, c_paul, 31, c_stoffregen, ", 1996", 0

str_code:
	db	"CODE", 0

str_data:
	db	"DATA", 0


cout_hh:
	ljmp	pm.cout
phex_hh:
	ljmp	pm.phex
pstr_hh:
	ljmp	pm.pstr
newline_hh:
	ljmp	pm.newline
pcstr_hh:
	ljmp	pm.pcstr
pint_hh:
	ljmp	pm.pint8

;---------------------------------------------------------;
;                                                         ;
;                    single step strings                  ;
;                                                         ;
;---------------------------------------------------------;

prompt4:
	db	"), or <",c_esc, "> to", c_quit, ": ",0

prompt8:	; "\nJump to memory location ("
	db	13, 31, c_jump, c_to, c_memory, c_location, " (", 0

abort:
	db	" ", 31, c_command, c_abort, "ed.", 14


sserr1:		; "\nyou must connect INT1 (pin 13) low to use single step\n"
	db	13, c_you, c_must, " connect INT1 (pin 13) low"
	db	c_to, c_use, c_single, c_step, 14

sserr2:		; "PAULMON2 not found at location 0013\n"
	db	c_paulmon, "2", c_not, c_found, c_at, c_location, " 0013", 14

sserr3:		; "Can not print interrupt vector at "
	db	31, c_can, c_not, c_print, c_interrupt, " vector", c_at, " ", 0

ssmsg:		; "Now running in single step mode:  <RET>= step, ?= Help\n\n"
	db	13, "Now", c_run, "ning", c_in, c_single, c_step, " mode:  "
	db	"<RET>=", c_step, ", ?= Help", 13, 14

sskip1:
	db	"Skipping", c_instruction, "-> ", 0
ssdmps1:
	db	13, 10, "Loc:  Int", c_ram, c_memory, " contents", 14
chaccs1:
	db	"New Acc Value: ", 0

help5txt:
		; "\n"
	db	13

		; "Single Step Command:\n"
	db	31, c_single, 31, c_step, 31, c_command, ":", 13

		; "<RET> run next instruction\n"
	db	"<RET> ", c_run, c_next, c_instruction, 13

		; " <SP> run next instruction\n"
	db	" <SP> ", c_run, c_next, c_instruction, 13

		; "'?''  print this help\n"
	db	" '?'  ", c_print, c_this, c_help, 13

		; " '.'  print register xxxs\n"
	db	" '.'  ", c_print, c_register, c_name, "s", 13

		; " 'R'  print special function registers\n"
	db	" 'R'  ", c_print, " special function", c_register, "s", 13

		; " 'H'  hex dump internal ram"
	db	" 'H'  ", c_hex, c_dump, c_internal, c_ram, 13

		; " 'S'  skip next instruction\n"
	db	" 'S'  ", c_skip, c_next, c_instruction, 13

		; " 'A'  Change xxx Acc value\n"
	db	" 'A'  ", c_change, c_the, " Acc value", 13

		; " 'Q' quit single step\n\n"
	db	" 'Q'  ", c_quit, c_single, c_step, 13, 14

squit:
	db	"Quit", 13, 10, 0

ssnames:
	db	"  ACC B C DPTR  R0 R1 R2 R3 R4 R5 R6 R7  SP"
	db	"   Addr  Instruction", 13, 10, 0

;---------------------------------------------------------;
;                                                         ;
;                    disassembler data                    ;
;                                                         ;
;---------------------------------------------------------;

mnu_tbl:
	db	"ACAL", 'L' + 128
	db	0
	db	"AD", 'D' + 128
	db	0
	db	"ADD", 'C' + 128
	db	"AJM", 'P' + 128
	db	"AN", 'L' + 128
	db	"CJN", 'E' + 128
	db	"CL", 'R' + 128
	db	"CP", 'L' + 128
	db	"D", 'A' + 128
	db	"DE", 'C' + 128
	db	"DI", 'V' + 128
	db	"DJN", 'Z' + 128
	db	"IN", 'C' + 128
	db	"J", 'B' + 128
	db	"JB", 'C' + 128
	db	"J", 'C' + 128
	db	"JM", 'P' + 128
	db	"JN", 'B' + 128
	db	"JN", 'C' + 128
	db	"JN", 'Z' + 128
	db	"J", 'Z' + 128
	db	"LCAL", 'L' + 128
	db	"LJM", 'P' + 128
	db	"MO", 'V' + 128
	db	"MOV", 'C' + 128
	db	"MOV", 'X' + 128
	db	"MU", 'L' + 128
	db	"NO", 'P' + 128
	db	"OR", 'L' + 128
	db	"PO", 'P' + 128
	db	"PUS", 'H' + 128
	db	"RE", 'T' + 128
	db	"RET", 'I' + 128
	db	"R", 'L' + 128
	db	"RL", 'C' + 128
	db	"R", 'R' + 128
	db	"RR", 'C' + 128
	db	"SET", 'B' + 128
	db	"SJM", 'P' + 128
	db	"SUB", 'B' + 128
	db	"SWA", 'P' + 128
	db	"XC", 'H' + 128
	db	"XCH", 'D' + 128
	db	"XR", 'L' + 128
	db	"??", '?' + 128

bitmnu:
	db	'P', '0' + 128
	db	"TCO", 'N' + 128
	db	'P', '1' + 128
	db	"SCO", 'N' + 128
	db	'P', '2' + 128
	db	'I', 'E' + 128
	db	'P', '3' + 128
	db	'I', 'P' + 128
	db	'C', '0' + 128
	db	"T2CO", 'N' + 128
	db	"PS", 'W' + 128
	db	'D', '8' + 128
	db	"AC", 'C' + 128
	db	'E' + '8' + 128
	db	'B' + 128
	db	'F' + '8' + 128
