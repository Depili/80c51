# Paulmon2

A customized version of Paulmon2 from https://www.pjrc.com/tech/8051/paulmon2.html

Paulmon2 has originally been written by Paul Stoffregen and released into public
domain.

This version has been customized for the needs of this project:
* Ported to the macro assembler AS http://john.ccac.rwth-aachen.de:8000/as/
	* Relabeled most of the inner labels in functions
* Cleaned up the whitespace usage
* Backported the 28 series eeprom code from paulmon2.0 to paulmon2.1
* Made all the memory routines go from bmem - emem and bflash to eflash
	* Cleaned the loops to use common increment code
* Made the intmem dump do 256 bytes
* TODO: T2 UART baud rate
* TODO: XOFF / XON for download?
