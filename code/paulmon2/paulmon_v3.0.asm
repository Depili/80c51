; PAULMON2, a user-friendly 8051 monitor, by Paul Stoffregen
; Please email comments, suggestions, bugs to paul@pjrc.com

; Version 2.1, flash rom changed from 28F256 (obsolete) to
;   the standard 39F512 algorithm (4 cycle write, 6 cycle erase).
;   Some code size improvements, contributed by Alexander B. Alexandrov
;   Download can now start from main menu prompt

; It's free.  PAULMON2 is in the public domain.  You may copy
; sections of code from PAULMON2 into your own programs, even
; for commercial purposes.  PAULMON2 should only be distributed
; free of charge, but may be bundled as 'value-added' with other
; products, such as development boards, CDROMs, etc.  Please
; distribute the PAULMON2.DOC file and other files, not just
; the object code!

; The PAULMON2.EQU and PAULMON2.HDR files contain valuable
; information that could help you to write programs for use
; with PAULMON2.

; PAULMON2 is in the public domain. PAULMON2 is distributed in
; the hope that it will be useful, but without any warranty;
; without even the implied warranty of merchantability or fitness
; for a particular purpose. 


; You are probably reading this code to see what it looks like
; and possibly learn something, or to modify it for some reason.
; Either is ok, but please remember that this code uses a number
; of tricks to cram all the functionality into just 4k.  As a
; result, the code can be difficult to read, and adding new
; features can be very difficult without growing beyond 4k.  To
; add or modify commands in PAULMON2, please consider using the
; "external command" functionality.  It is easier to develop
; new commands this way, and you can distribute them to other
; users.  Email paul@pjrc.com if you have new PAULMON2
; commands to contribute to others.  Details about adding new
; commands to PAULMON2 (with examples) can be found at:

; http://www.pjrc.com/tech/8051/pm2_docs/addons.html


;---------------------------------------------------------;
;							  ;
;	    PAULMON2's default configuration		  ;
;							  ;
;---------------------------------------------------------;

; PAULMON2 should be assembled using the modified AS31 assembler,
; originally written by Ken Stauffer, many small changes by Paul
; Stoffregen.  This free assembler is available on the web at
; http://www.pjrc.com/tech/8051/index.html
; As well, these web pages have a fill-out form which makes it
; very easy to custom configure PAULMON2.  Using this form will
; edit the code for you, run the AS31 assmebler, and send you the
; object code to program into your chip.

	CPU	8051
	INCLUDE	../p80c51fa.inc
	INCLUDE paulmon_v3.0.inc
	INCLUDE paulmon_strings_v3.0.inc

; These two parameters control where PAULMON2 will be assembled,
; and where it will attempt to LJMP at the interrupt vector locations.

	.equ	base, paulmon2			; location for PAULMON2


; To set the baud rate, use this formula or set to 0 for auto detection
; baud_const = 256 - (crystal / (12 * 16 * baud))

	.equ	uart_t2, 1			; Use timer 2 for baud rate


; Timer 1 constants
	.equ	baud_const, 0			; automatic baud rate detection
;	.equ	baud_const, 255			; 57600 baud w/ 11.0592 MHz
;	.equ	baud_const, 253			; 19200 baud w/ 11.0592 MHz
;	.equ	baud_const, 252			; 19200 baud w/ 14.7456 MHz
;	.equ	baud_const, 243			; 4808 baud w/ 12 MHz
;	.equ	baud_const, 247			; 9600 baud w/ 16 MHz

; Timer 2 constants
	.equ	baud_9600, 0CCh
	.equ	baud_14400, 0DDh
	.equ	baud_19200, 0E6h
	.equ	baud_38400, 0f3h
	.equ	baud_rate, baud_38400


	.equ	line_delay, 6		;num of char times to pause during uploads

; About download speed: when writing to ram, PAULMON2 can accept data
; at the maximum baud rate (baud_const=255 or 57600 baud w/ 11.0592 MHz).
; Most terminal emulation programs introduce intentional delays when
; sending ascii data, which you would want to turn off for downloading
; larger programs into ram.  For Flash ROM, the maximum speed is set by
; the time it takes to program each location... 9600 baud seems to work
; nicely for the AMD 28F256 chip.  The "character pacing" delay in a
; terminal emulation program should be sufficient to download to flash
; rom and any baud rate.  Some flash rom chips can write very quickly,
; allowing high speed baud rates, but other chips can not.  You milage
; will vary...


; Flash ROM parameters.	 If "has_flash" is set to zero, all flash rom
; features are turned off, otherwise "bflash" and "eflash" should specify
; the memory range which is flash rom.	Though AMD doesn't suggest it,
; you may be able to map only a part of the flash rom with your address
; decoder logic (and not use the rest), but you should be careful that
; "bflash" and "eflash" don't include and memory which is NOT flash rom
; so that the erase algorithm won't keep applying erase pulses until it
; finally gives up (which will stress the thin oxide and degrade the
; flash rom's life and reliability).  "erase_pin" allows you to specify
; the bit address for a pin which (if held low) will tell PAULMON2 to
; erase the flash rom chip when it starts up.  This is useful if you
; download programs with the "start-up" headers on them and the code you've
; put in the flash rom crashes!

	.equ	erase_pin, 0			; 00 = disable erase pin feature
;	.equ	erase_pin, 0B5H			; B5 = pin 15, P3.5 (T1)


; Development Board Wiring, http://www.pjrc.com/tech/8051/
; wiring is not a simple A0 to A0... works fine, but requires the
; special Flash ROM programming addresses to be encoded.

; Flash: A15 A14 A13 A12 A11 A10  A9  A8  A7  A6  A5  A4  A3  A2  A1  A0
; 8051:   -  A14 A13  A1  A9  A8 A10 A11  A0  A3  A2  A4  A5  A6  A7 A12
;
; 0x5555  0   1   0   1   0   1   0   1   0   1   0   1   0   1   0   1
; 0x595A  0   1   0   1   1   0   0   1   0   1   0   1   1   0   1   0
;
; 0x2AAA  0   0   1   0   1   0   1   0   1   0   1   0   1   0   1   0
; 0x26A5  0   0   1   0   0   1   1   0   1   0   1   0   0   1   0   1

; sets the base address to add to the flash memory register addresses.
	.equ	flash_base, bflash

	.equ	flash_en1_addr, 05555H + flash_base
;	.equ	flash_en1_addr, 0595AH + flash_base
	.equ	flash_en1_data, 0AAH

	.equ	flash_en2_addr, 02AAAH + flash_base
;	.equ	flash_en2_addr, 026A5H + flash_base
	.equ	flash_en2_data, 055H

	.equ	flash_wr_addr, 05555H + flash_base
;	.equ	flash_wr_addr, 0x595AH + flash_base
	.equ	flash_wr_data, 0A0H

	.equ	flash_er1_addr, 05555H + flash_base
;	.equ	flash_er1_addr, 0595AH + flash_base
	.equ	flash_er1_data, 080H

	.equ	flash_er2_addr, 05555H + flash_base
;	.equ	flash_er2_addr, 0595AH + flash_base
	.equ	flash_er2_data, 010H


; Please note... much of the memory management code only looks at the
; upper 8 bits of an address, so it's not a good idea to somehow map
; your memory chips (with complex address decoding logic) into chunks
; less than 256 bytes.	In other words, only using a piece of a flash
; rom chip and mapping it between C43A to F91B would confuse PAULMON2
; (as well as require quit a bit of address decoding logic circuitry)


; timing parameters for AMD Flash ROM 28F256.  These parameters
; and pretty conservative and they seem to work with crystals
; between 6 MHz to 24 MHz... (tested with AMD 28F256 chips only)
; unless you know this is a problem, it is probably not a good
; idea to fiddle with these.

;	.equ	pgmwait, 10			; 22.1184 MHz crystal assumed
	.equ	pgmwait, 19			; 11.0592 MHz
	.equ	verwait, 5
;	.equ	erwait1, 40			; fourty delays @22.1184
	.equ	erwait1, 20			; twenty delays for 11.0592 MHz
	.equ	erwait2, 229			; each delay .5 ms @22.1184MHz

; cin__filter
	.equ	char_delay, 4			; number of char xmit times to wait

;---------------------------------------------------------;
;							  ;
;		     Interrupt Vectors			  ;
;  (and little bits of code crammed in the empty spaces)  ;
;							  ;
;---------------------------------------------------------;

	org	base
RESET:
	ljmp	poweron		; reset vector

	org	base + 3h
ISR_INT0:
	ljmp	vector + 3h	; ext int0 vector

; Moves address from r6, r7 to dptr
r6r7todptr:
	mov	dpl, r6
	mov	dph, r7
	ret

	org	base + 0bh
ISR_TF0:
	ljmp	vector + 0bh	; timer0 vector

; Stores dptr in r6 and r7
dptrtor6r7:
	mov	r6, dpl
	mov	r7, dph
	ret

	org	base + 13h
ISR_INT1:
	ljmp	vector + 13h	; ext int1 vector

; seems kinda trivial, but each time
; this appears in code, it takes 4
; bytes, but an acall takes only 2
dash:
	mov	a, #'-'
	ajmp	cout
	nop

	org	base + 1bh
ISR_TF1:
	ljmp	vector + 1bh	; timer1 vector

; Print a space
cout_sp:
	acall	cout
	ajmp	space
	nop

	org	base + 23h
ISR_UART:
	ljmp	vector + 23h	; uart vector

dash_sp:
	acall	dash
	ajmp	space
	nop

	org	base + 2bh
ISR_TF2:
	ljmp	vector + 2bh	; timer2 vector (8052)


	org	base + 33h
ISR_PCA:
	ljmp	vector + 33h	; PCA vector (80c51FA)

;-------------------------------------------------------;
;							;
;	The jump table for user programs to call	;
;	      subroutines within PAULMON		;
;							;
;-------------------------------------------------------;

	org	base + 36h	; never change this line!!  Other
				; programs depend on these locations
				; to access paulmon2 functions

	ajmp	phex1		; 36
	ajmp	cout		; 38
	ajmp	cin		; 3A
	ajmp	phex		; 3C
	ajmp	phex16		; 3E
	ajmp	pstr		; 40
	ajmp	ghex		; 42
	ajmp	ghex16		; 44
	ajmp	esc		; 46
	ajmp	upper		; 48
	ljmp	autobaud	; 4A
pcstr_h:
	ljmp	pcstr		; 4D
	ajmp	newline		; 50
	ljmp	lenstr		; 52
	ljmp	pint8u		; 55
	ljmp	pint8		; 58
	ljmp	pint16u		; 5B
	ljmp	smart_wr	; 5E
	ljmp	prgm		; 61
	ljmp	erall		; 64
	ljmp	find		; 67
cin_filter_h:
	ljmp	cin_filter	; 6A
	ajmp	asc2hex		; 6D
;	ljmp	erblock_pm21	; 6F


;---------------------------------------;
;					;
;	Subroutines for serial I/O	;
;					;
;---------------------------------------;


cin:
	jnb	ri, cin
	clr	ri
	mov	a, sbuf
	ret

dspace:
	acall	space
space:
	mov	a, #' '
cout:
	jnb	ti, cout
	clr	ti		; clr ti before the mov to sbuf!
	mov	sbuf, a
	ret

; clearing ti before reading sbuf takes care of the case where
; interrupts may be enabled... if an interrupt were to happen
; between those two instructions, the serial port will just
; wait a while, but in the other order and the character could
; finish transmitting (during the interrupt routine) and then
; it would be cleared and never set again by the hardware, causing
; the next call to cout to hang forever!

newline2:			; print two newlines
	acall	newline
newline:
	push	acc		; print one newline
	mov	a, #CR_char
	acall	cout
	mov	a, #10
	acall	cout
	pop	acc
	ret


	; get 2 digit hex number from serial port
	; c = set if ESC pressed, clear otherwise
	; psw.5 = set if return w/ no input, clear otherwise
	; Destroys r2, r3
ghex:
ghex8:
	clr	psw.5
.read_char:
	acall	cin_filter_h			; get first digit
	acall	upper
	cjne	a, #esc_char, .check_cr
.set_carry:
	setb	c
	clr	a
	ret
.check_cr:
	cjne	a, #CR_char, .process_char
	setb	psw.5
	clr	c
	clr	a
	ret
.process_char:
	mov	r2, a
	acall	asc2hex
	jc	.read_char
	xch	a, r2				; r2 will hold hex value of 1st digit
	acall	cout
.char_2:
	acall	cin_filter_h			; get second digit
	acall	upper
	cjne	a, #esc_char, .check_cr_2
	sjmp	.set_carry
.check_cr_2:
	cjne	a, #CR_char, .check_backspace
	mov	a, r2
	clr	c
	ret
.check_backspace:
	cjne	a, #backspace, .check_nak
.erase:
	acall	cout
	sjmp	.read_char
.check_nak:
	cjne	a, #21, .finalize		; NAK
	sjmp	.erase
.finalize:
	mov	r3, a
	acall	asc2hex
	jc	.char_2
	xch	a, r3
	acall	cout
	mov	a, r2
	swap	a
	orl	a, r3
	clr	c
	ret


	; carry set if esc pressed
	; psw.5 set if return pressed w/ no input
	; Destroys r2, r3, r4, r5
ghex16:
	mov	r2, #0				; start out with 0
	mov	r3, #0
	mov	r4, #4				; number of digits left
	clr	psw.5

.loop:
	acall	cin_filter_h
	acall	upper
	cjne	a, #esc_char, .check_backspace
	setb	c				; handle esc key
	clr	a
	mov	dph, a
	mov	dpl, a
	ret
.check_backspace:
	cjne	a, #backspace, .check_delete
	sjmp	.handle_backspace
.check_delete:
	cjne	a, #delete_char, .check_cr  	; handle backspace
.handle_backspace:
	cjne	r4, #4, .output	  		; have they entered anything yet?
	sjmp	.loop
.output:
	acall	cout
	acall	ghex16y
	inc	r4
	sjmp	.loop
.check_cr:
	cjne	a, #CR_char, .no_cr	  	; return key
	mov	dph, r3
	mov	dpl, r2
	cjne	r4, #4, .have_input
	clr	a
	mov	dph, a
	mov	dpl, a
	setb	psw.5
.have_input:
	clr	c
	ret
.no_cr:
	mov	r5, a		  		; keep copy of original keystroke
	acall	asc2hex
	jc	.loop
	xch	a, r5
	lcall	cout
	mov	a, r5
	push	acc
	acall	ghex16x
	pop	acc
	add	a, r2
	mov	r2, a
	clr	a
	addc	a, r3
	mov	r3, a
	djnz	r4, .loop
	clr	c
	mov	dpl, r2
	mov	dph, r3
	ret

ghex16x:
	; multiply r3-r2 by 16 (shift left by 4)
	mov	a, r3
	swap	a
	anl	a, #11110000b
	mov	r3, a
	mov	a, r2
	swap	a
	anl	a, #00001111b
	orl	a, r3
	mov	r3, a
	mov	a, r2
	swap	a
	anl	a, #11110000b
	mov	r2, a
	ret

ghex16y:
	; divide r3-r2 by 16 (shift right by 4)
	mov	a, r2
	swap	a
	anl	a, #00001111b
	mov	r2, a
	mov	a, r3
	swap	a
	anl	a, #11110000b
	orl	a, r2
	mov	r2, a
	mov	a, r3
	swap	a
	anl	a, #00001111b
	mov	r3, a
	ret


asc2hex:
	; carry set if invalid input
	add	a, #208
	jnc	.not
	add	a, #246
	jc	.maybe
	add	a, #10
	clr	c
	ret
.maybe:
	add	a, #249
	jnc	.not
	add	a, #250
	jc	.not
	add	a, #16
	clr	c
	ret
.not:
	setb	c
	ret


; Highly code efficient resursive call phex contributed
; by Alexander B. Alexandrov <abalex@cbr.spb.ru>

phex:
	; Output the byte as hex
	acall	.out_digit
.out_digit:
	swap	a		; SWAP A will be twice => A unchanged
phex1:
	; Output the low nibble as hex
	push	acc
	anl	a, #15
	add	a, #090H	; acc is 0x9X, where X is hex digit
	da	a		; if A to F, C=1 and lower four bits are 0..5
	addc	a, #040H
	da	a
	acall	cout
	pop	acc
	ret

; the old code... easier to understand
;	push	acc
;	swap	a
;	anl	a, #15
;	add	a, #246
;	jnc	phex_b
;	add	a, #7
; phex_b:add	a, #58
;	acall	cout
;	pop	acc
; phex1:	push	acc
;	anl	a, #15
;	add	a, #246
;	jnc	phex_c
;	add	a, #7
; phex_c:add	a, #58
;	acall	cout
;	pop	acc
;	ret



phex16:
	push	acc
	mov	a, dph
	acall	phex
	mov	a, dpl
	acall	phex
	pop	acc
	ret


; a not so well documented feature of pstr is that you can print
; multiple consecutive strings without needing to reload dptr
; (which takes 3 bytes of code!)... this is useful for inserting
; numbers or spaces between strings.

pstr:
	push	acc
.loop:
	clr	a
	movc	a, @a+dptr
	inc	dptr
	jz	.end
	mov	c, acc.7
	anl	a, #07FH
	acall	cout
	jc	.end
	sjmp	.loop
.end:
	pop	acc
	ret

; converts the ascii code in Acc to uppercase, if it is lowercase

; Code efficient (saves 6 byes) upper contributed
; by Alexander B. Alexandrov <abalex@cbr.spb.ru>

upper:
	cjne	a, #97, .test1
.test1:
	jc	.end		; end if acc < 97
	cjne	a, #123, .test2
.test2:
	jnc	.end		; end if acc >= 123
	add	a, #224		; 96 > ACC > 123; convert to uppercase
.end:
	ret



lenstr:
	mov	r0, #0	  	; returns length of a string in r0
	push	acc
.loop:
	clr	a
	movc	a, @a+dptr
	jz	.end
	mov	c, acc.7
	inc	r0
	Jc	.end
	inc	dptr
	sjmp	.loop
.end:
	pop	acc
	ret


esc:
	; checks to see if <ESC> is waiting on serial port
	; C=clear if no <ESC>, C=set if <ESC> pressed
	; buffer is flushed
	push	acc
	clr	c
	jnb	ri, .end
	mov	a,sbuf
	cjne	a,#esc_char, .no_carry
	setb	c
.no_carry:
	clr	ri
.end:
	pop	acc
	ret


;---------------------------------------------------------;
;							  ;
;    The 'high-level' stuff to interact with the user	  ;
;							  ;
;---------------------------------------------------------;


menu:
	; first we print out the prompt, which isn't as simple
	; as it may seem, since external code can add to the
	; prompt, so we've got to find and execute all of 'em.
	mov	dptr, #prompt1		; give 'em the first part of prompt
	acall	pcstr_h
	mov	a, r7
	acall	phex
	mov	a, r6
	acall	phex
	; mov	 dptr, #prompt2
	acall	pstr

	; now we're finally past the prompt, so let's get some input
	acall	cin_filter_h		; get the input, finally
	cjne	a, #':', .no_colon
	acall	dnld.now
	sjmp	menu
.no_colon:
	acall	upper

	; push return address onto stack so we can just jump to the program
	mov	b, #(menu & 255)	; we push the return address now,
	push	b			; to save code later...
	mov	b, #(menu >> 8)		; if bogus input, just ret for
	push	b			; another prompt.


	; first we'll look through memory for a program header that says
	; it's a user installed command which matches what the user pressed

	; user installed commands need to avoid changing R6/R7, which holds
	; the memory pointer.  The stack pointer can't be changed obviously.
	; all the other general purpose registers should be available for
	; user commands to alter as they wish.

	mov	b, a			; now search for external commands...
	acall	find_first
.find_ext:
	jnc	.no_more_ext		; searched all the commands?
	mov	dpl, #4
	clr	a
	movc	a,@a+dptr
	cjne	a, #254, .next_ext	; only FE is an ext command
	inc	dpl
	clr	a
	movc	a,@a+dptr
	cjne	a, b, .next_ext		; only run if they want it
	acall	space
	mov	dpl, #32
	acall	pstr			; print command name
	acall	newline
	mov	dpl, #64
	clr	a
	jmp	@a+dptr			; take a leap of faith and jump to it!
.next_ext:
	acall	find.next
	jc	.find_ext

.no_more_ext:
	mov	a, b

	; since we didn't find a user installed command, use the builtin ones

.help:
	cjne	a, #help_key, .dir
	mov	dptr, #help_cmd2
	acall	pcstr_h
	ajmp	help
.dir:
	cjne	a, #dir_key, .run
	mov	dptr, #dir_cmd
	acall	pcstr_h
	ajmp	dir
.run:
	cjne	a, #run_key, .download
	mov	dptr, #run_cmd
	acall	pcstr_h
	ajmp	run
.download:
	cjne	a, #dnld_key, .upload
	mov	dptr, #dnld_cmd
	acall	pcstr_h
	ajmp	dnld
.upload:
	cjne	a, #upld_key, .new_location
	mov	dptr, #upld_cmd
	acall	pcstr_h
	ajmp	upld
.new_location:
	cjne	a, #nloc_key, .jump
	mov	dptr, #nloc_cmd
	acall	pcstr_h
	ajmp	nloc
.jump:
	cjne	a, #jump_key, .dump
	mov	dptr, #jump_cmd
	acall	pcstr_h
	ajmp	jump
.dump:
	cjne	a, #dump_key, .edit
	mov	dptr, #dump_cmd
	acall	pcstr_h
	ajmp	dump
.edit:
	cjne	a, #edit_key, .clear_mem
	mov	dptr, #edit_cmd
	acall	pcstr_h
	ajmp	edit
.clear_mem:
	cjne	a, #clrm_key, .erase_flash
	mov	dptr, #clrm_cmd
	acall	pcstr_h
	ajmp	clrm
.erase_flash:
	cjne	a, #erfr_key, .dump_intmem
	mov	a, #has_flash
	jz	.end
	mov	dptr, #erfr_cmd
	acall	pcstr_h
	ajmp	erfr
.dump_intmem:
	cjne	a, #intm_key, .dump_strings
	mov	dptr, #intm_cmd
	acall	pcstr_h
	ljmp	intm
.dump_strings
	cjne	a, #dump_strings_key, .invalid
	mov	dptr, #dump_strings_cmd
	acall	pcstr_h
	ljmp	dump_strings
.invalid:

	; invalid input, no commands to run...
.end:
	; at this point, we have not found
	; anything to run, so we give up.
	; remember, we pushed menu, so newline
	; will just return to menu.
	ajmp	newline


;..........................................................

;---------------------------------------------------------;

; dnlds1 = "Begin ascii transfer of Intel hex file, or ESC to abort"
; dnlds2 = "Download aborted"
; dnlds3 = "Download completed"


; 16 byte parameter table: (eight 16 bit values)
;  *   0 = lines received
;  *   1 = bytes received
;  *   2 = bytes written
;  *   3 = bytes unable to write
;  *   4 = incorrect checksums
;  *   5 = unexpected begin of line
;  *   6 = unexpected hex digits (while waiting for bol)
;  *   7 = unexpected non-hex digits (in middle of a line)



dnld:
	mov	dptr, #dnlds1		 
	acall	pcstr_h				; "Begin ascii transfer of Intel hex file, or ESC to abort"
	acall	dnld_init

	; look for begining of line marker ':'
.loop:
	acall	cin
	cjne	a, #esc_char, .check_colon	; Test for escape
	ajmp	.esc

.check_colon:
	cjne	a, #':', .check_hex
	sjmp	.init
.check_hex:
	; check to see if it's a hex digit, error if it is
	acall	asc2hex
	jc	.loop
	mov	r1, #6
	acall	dnld_inc
	sjmp	.loop

.now:
	; entry point for main menu detecting ":" character
	mov	a, #'^'
	acall	cout
	acall	dnld_init

.init:
	mov	r1, #0
	acall	dnld_inc

	; begin taking in the line of data
.line_start:
	; mov	a, #'.'
	; acall	cout
	mov	r4, #0				; r4 will count up checksum
	acall	dnld_ghex
	mov	r0, a				; R0 = # of data bytes
	mov	a, #'.'
	acall	cout
	acall	dnld_ghex
	mov	dph, a				; High byte of load address
	acall	dnld_ghex
	mov	dpl, a				; Low byte of load address
	acall	dnld_ghex			; Record type
	cjne	a, #1, +			; End record?
	sjmp	.end
+	jnz	.unknown			; is it a unknown record type???
.data_loop:
	mov	a, r0
	jz	.get_cksum
	acall	dnld_ghex			; Get data byte
	mov	r2, a
	mov	r1, #1
	acall	dnld_inc			; count total data bytes received
	mov	a, #xoff
	acall	cout
	mov	a, r2
	lcall	smart_wr			; c=1 if an error writing
	clr	a
	addc	a, #2
	mov	r1, a
;     2 = bytes written
;     3 = bytes unable to write
	acall	dnld_inc
	mov	a, #xon
	acall	cout
	inc	dptr
	djnz	r0, .data_loop
.get_cksum:
	acall	dnld_ghex			; get checksum
	mov	a, r4
	jz	.loop				; should always add to zero
.sumerr:
	mov	r1, #4
	acall	dnld_inc			; all we can do it count # of cksum errors
	sjmp	.loop

.unknown:
	; handle unknown line type
	mov	a, r0
	jz	.get_cksum			; skip data if size is zero
.unknown_loop:
	acall	dnld_ghex			; consume all of unknown data
	djnz	r0, .unknown_loop
	sjmp	.get_cksum

.end:
	; handles the proper end-of-download marker
	mov	a, r0
	jz	.end_3				; should usually be zero
	acall	dnld_ghex			; consume all of useless data
	djnz	r0, .unknown_loop
.end_3:
	acall	dnld_ghex			; get the last checksum
	mov	a, r4
	jnz	.sumerr
	acall	.delay
	mov	dptr, #dnlds3
	acall	pcstr_h		   		; "download went ok..."
	; consume any cr or lf character that may have been
	; on the end of the last line
	jnb	ri, dnld_sum
	acall	cin
	sjmp	dnld_sum



.esc:
	; handle esc received in the download stream
	acall	.delay
	mov	dptr, #dnlds2	 
	acall	pcstr_h		   		; "Download aborted."
	sjmp	dnld_sum

.delay:
	; a short delay since most terminal emulation programs
	; won't be ready to receive anything immediately after
	; they've transmitted a file... even on a fast Pentium(tm)
	; machine with 16550 uarts!
	mov	r0, #0
-	mov	r1, #0
	djnz	r1, $				; roughly 128k cycles, appox 0.1 sec
	djnz	r0, -
	ret

dnld_inc:
	; increment parameter specified by R1
	; note, values in Acc and R1 are destroyed
	mov	a, r1
	anl	a, #00000111b			; just in case
	rl	a
	add	a, #dnld_parm
	mov	r1, a				; now r1 points to lsb
	inc	@r1
	mov	a, @r1
	jnz	.end
	inc	r1
	inc	@r1
.end:
	ret

dnld_gp:
	; get parameter, and inc to next one (@r1)
	; carry clear if parameter is zero.
	; 16 bit value returned in dptr
	setb	c
	mov	dpl, @r1
	inc	r1
	mov	dph, @r1
	inc	r1
	mov	a, dpl
	jnz	.end
	mov	a, dph
	jnz	.end
	clr	c
.end:
	ret



; a spacial version of ghex just for the download.  Does not
; look for carriage return or backspace.

; Handles ESC key by poping the return address (I know, nasty,
; but it saves many bytes of code in this 4k ROM) and then
; jumps to the esc key handling.

; This ghex doesn't echo characters, and if it sees ':', it
; pops the return and jumps to an error handler for ':' in
; the middle of a line.

; Non-hex digits also jump to error handlers, depending on
; which digit.
	  
dnld_ghex:
.start:
	acall	cin
	acall	upper
	cjne	a, #esc_char, .colon
.esc:
	pop	acc
	pop	acc
	sjmp	dnld.esc
.colon:
	cjne	a, #':', .line_start
.error_newline:
	mov	r1, #5			; handle unexpected beginning of line
	acall	dnld_inc
	pop	acc
	pop	acc
	ajmp	dnld.line_start		; and now we're on a new line!
.line_start:
	acall	asc2hex
	jnc	.valid
	mov	r1, #7
	acall	dnld_inc
	sjmp	.start
.valid:
	mov	r2, a			; keep first digit in r2
.loop:
	acall	cin
	acall	upper
	cjne	a, #esc_char, .no_esc
	sjmp	.esc
.no_esc:
	cjne	a, #':', .no_colon
	sjmp	.error_newline
.no_colon:
	acall	asc2hex
	jnc	.end
	mov	r1, #7
	acall	dnld_inc
	sjmp	.loop
.end:
	xch	a, r2
	swap	a
	orl	a, r2
	mov	r2, a
	add	a, r4			; add into checksum
	mov	r4, a
	mov	a, r2			; return value in acc
	ret

; dnlds4 =  "Summary:"
; dnlds5 =  " lines received"
; dnlds6a = " bytes received"
; dnlds6b = " bytes written"

dnld_sum:
	; print out download summary
	mov	a, r6
	push	acc
	mov	a, r7
	push	acc
	mov	dptr, #dnlds4
	acall	pcstr_h
	mov	r1, #dnld_parm
	mov	r6, #dnlds5 & 255
	mov	r7, #dnlds5 >> 8
	acall	dnld_item_always
	mov	r6, #dnlds6a & 255
	mov	r7, #dnlds6a >> 8
	acall	dnld_item_always
	mov	r6, #dnlds6b & 255
	mov	r7, #dnlds6b >> 8
	acall	dnld_item_always

	; now print out error summary
	mov	r2, #5
.loop:
	acall	dnld_gp
	jc	.errors			; any errors?
	djnz	r2, .loop
	; no errors, so we print the nice message
	mov	dptr, #dnlds13
	acall	pcstr_h
	sjmp	.no_errors

.errors:
	; there were errors, so now we print 'em
	mov	dptr, #dnlds7
	acall	pcstr_h
	; but let's not be nasty... only print if necessary
	mov	r1, #(dnld_parm + 6)
	mov	r6, #dnlds8 & 255
	mov	r7, #dnlds8 >> 8
	acall	dnld_item_maybe
	mov	r6, #dnlds9 & 255
	mov	r7, #dnlds9 >> 8
	acall	dnld_item_maybe
	mov	r6, #dnlds10 & 255
	mov	r7, #dnlds10 >> 8
	acall	dnld_item_maybe
	mov	r6, #dnlds11 & 255
	mov	r7, #dnlds11 >> 8
	acall	dnld_item_maybe
	mov	r6, #dnlds12 & 255
	mov	r7, #dnlds12 >> 8
	acall	dnld_item_maybe
.no_errors:
	pop	acc
	mov	r7, a
	pop	acc
	mov	r6, a
	ajmp	newline


dnld_item_maybe:
	acall	dnld_gp		; error conditions
	jnc	.skip
.print:
	acall	space
	lcall	pint16u
	acall	r6r7todptr
	acall	pcstr_h
.skip:
	ret

dnld_item_always:
	acall	dnld_gp		; non-error conditions
	sjmp	dnld_item_maybe.print


dnld_init:
	; init all dnld parms to zero.
	mov	r0, #dnld_parm
.loop	mov	@r0, #0
	inc	r0
	cjne	r0, #dnld_parm + 16, .loop
	ret


; dnlds7:  = "Errors:"
; dnlds8:  = " bytes unable to write"
; dnlds9:  = " bad checksums"
; dnlds10: = " unexpected begin of line"
; dnlds11: = " unexpected hex digits"
; dnlds12: = " unexpected non-hex digits"
; dnlds13: = "No errors detected"



; ---------------------------------------------------------;


jump:
	mov	dptr, #prompt8
	acall	pcstr_h
	acall	r6r7todptr
	acall	phex16
	mov	dptr, #prompt4
	acall	pcstr_h
	acall	ghex16
	jb	psw.5, ++
	jnc	+
	ajmp	abort2
+	acall	dptrtor6r7
+	acall	newline
	mov	dptr, #runs1
	acall	pcstr_h
	acall	r6r7todptr

jump_doit:
	; jump to user code @dptr (this used by run command also)
	clr	a
	mov	psw, a
	mov	b, a
	mov	r0, #7
.loop:
	mov	@r0, a		; clear r7 to r1
	djnz	r0, .loop	; clear r0
	mov	sp, #8		; start w/ sp=7, like a real reset
	push	acc		; unlike a real reset, push 0000
	push	acc		; in case they end with a RET
	jmp	@a+dptr


;---------------------------------------------------------;

dump:	
	mov	r2, #16		; number of lines to print
	acall	newline2
.loop:
	acall	r6r7todptr
	acall	phex16		; tell 'em the memory location
	mov	a, #':'
	acall	cout_sp
	mov	r3, #16		; r3 counts # of bytes to print
	acall	r6r7todptr
.hex_loop:
	clr	a
	movc	a, @a+dptr
	inc	dptr
	acall	phex		; print each byte in hex
	acall	space
	djnz	r3, .hex_loop
	acall	dspace		; print a couple extra space
	mov	r3, #16
	acall	r6r7todptr
.ascii_loop:
	clr	a
	movc	a, @a+dptr
	inc	dptr
	anl	a, #01111111b	; avoid unprintable characters
	cjne	a, #127, +
	clr	a		; avoid 127/255 (delete/rubout) char
+	add	a, #224
	jc	.output
	clr	a		; avoid control characters
.output:
	add	a, #32
	acall	cout
	djnz	r3, .ascii_loop
	acall	newline
;	acall	line_dly
	acall	dptrtor6r7
	acall	esc
	jc	.end
	djnz	r2, .loop	; loop back up to print next line
.end:
	ajmp	newline

;---------------------------------------------------------;
; Simple memory editor

edit:
	; edit external ram...
	mov	dptr, #edits1
	acall	pcstr_h
	acall	r6r7todptr
.input:
	acall	phex16
	mov	a, #':'
	acall	cout_sp
	mov	a, #'('
	acall	cout
	acall	dptrtor6r7
	clr	a
	movc	a, @a+dptr
	acall	phex
	mov	dptr, #prompt10
	acall	pcstr_h
	acall	ghex
	jb	psw.5, .end
	jc	.end
	acall	r6r7todptr
	lcall	smart_wr
	acall	newline
	acall	r6r7todptr
	inc	dptr
	acall	dptrtor6r7
	ajmp	.input
.end:
	mov	dptr, #edits2
	ajmp	pcstr_h

;---------------------------------------------------------;
; List programs

dir:
	mov	dptr, #prompt9
	acall	pcstr_h
	mov	r0, #21
	mov	r5, #0
-	acall	space			; 21 x space
	djnz	r0, -
	; mov	dptr, #prompt9b
	acall	pcstr_h
	acall	find_first		; find the next program in memory
.loop:
	jc	.print_name
.end:
	ajmp	newline			; we're done if no more found
.print_name:
	acall	dspace
	mov	dpl, #32		; print its name
	acall	pstr
	mov	dpl, #32		; how long is the name
	acall	lenstr
	mov	a, #33
	clr	c
	subb	a, r0
	mov	r0, a
	mov	a, #' '			; print the right # of spaces
-	acall	cout
	djnz	r0, -
	mov	dpl, #0
	acall	phex16			; print the memory location
	mov	r0, #6
	mov	a, #' '
-	acall	cout
	djnz	r0, -
	mov	dpl, #4			; now figure out what type it is
	clr	a
	movc	a, @a+dptr
	mov	r2, dph			; save this, we're inside a search
.check_extern:
	cjne	a, #254, .check_startup
	mov	dptr, #type1		; it's an external command
	sjmp	.print
.check_startup:
	cjne	a, #253, .check_normal
.type4:
	mov	dptr, #type4		; it's a startup routine
	sjmp	.print
.check_normal:
	cjne	a, #35, .type4_check
	mov	dptr, #type2		; it's an ordinary program
	sjmp	.print
.type4_check:
	cjne	a, #249, .unknown
	sjmp	.type4
.unknown:
	mov	dptr, #type5		; who knows what the hell it is

.print:
	acall	pcstr_h		   	; print out the type
	mov	dph, r2			; go back and find the next one
	acall	newline
.next:
	acall	find.next
	sjmp	.loop

; type1 = Ext Command
; type4 = Startup
; type2 = Program
; type5 = ???

;---------------------------------------------------------;
; Run a program with a header

run:   
	acall	newline2
	mov	r2, #255		; first print the menu, count items
	acall	find_first
.loop:
	jnc	.found_all
	mov	dpl, #4
	clr	a
	movc	a, @a+dptr
	orl	a, #00000011b
	cpl	a
	jz	.find_next		; this one doesn't run... find next
	acall	dspace
	inc	r2
	mov	a, #'A'			; print the key to press
	add	a, r2
	acall	cout_sp
	acall	dash_sp
	mov	dpl, #32
	acall	pstr			; and the command name
	acall	newline
.find_next:
	acall	find.next
	jc	.loop
.found_all:
	cjne	r2, #255, .ask_to_run	; are there any to run??
	mov	dptr, #prompt5
	ajmp	pcstr_h
.ask_to_run:
	mov	dptr, #prompt3		; ask the big question!
	acall	pcstr_h
	mov	a, #'A'
	acall	cout
	acall	dash
	mov	a, #'A'			; such user friendliness...
	add	a, r2			; even tell 'em the choices
	acall	cout
	mov	dptr, #prompt4
	acall	pcstr_h
	acall	cin_filter_h
	cjne	a, #esc_char, .no_esc	; they they hit <ESC>
	ajmp	newline
.no_esc:
	mov	r3, a
	mov	a, #31
	clr	c
	subb	a, r2
	mov	a, r3
	jc	+
	acall	upper
+	acall	cout
	mov	r3, a
	acall	newline
	; check to see if it's under 32, if so convert to uppercase
	mov	a, r3
	add	a, #(256 - 'A')
	jnc	.ask_to_run		; if they typed less than 'A'
	mov	r3, a			; R3 has the number they typed
	mov	a, r2			; A=R2 has the maximum number
	clr	c
	subb	a, r3
	jc	.ask_to_run		; if they typed over the max
	inc	r3
	acall	find_first
.exec_loop:
	jnc	.end			; Shouldn't ever do this jump!
	mov	dpl, #4
	clr	a
	movc	a, @a+dptr
	orl	a, #00000011b
	cpl	a
	jz	.exec_next		; this one doesn't run... find next
	djnz	r3, .exec_next		; count til we find the one they want
	acall	newline
	mov	dpl, #64
	ajmp	jump_doit
.exec_next:
	acall	find.next
	sjmp	.exec_loop
.end:
	ret

;---------------------------------------------------------;
; Print help text for commands

help:
	mov	dptr, #help1txt
	acall	pcstr_h
	mov	r4, #help_key
	mov	dptr, #help_cmd
	acall	.print
	mov	r4, #dir_key
	;mov	 dptr, #dir_cmd
	acall	.print
	mov	r4, #run_key
	;mov	 dptr, #run_cmd
	acall	.print
	mov	r4, #dnld_key
	;mov	 dptr, #dnld_cmd
	acall	.print
	mov	r4, #upld_key
	;mov	 dptr, #upld_cmd
	acall	.print
	mov	r4, #nloc_key
	;mov	 dptr, #nloc_cmd
	acall	.print
	mov	r4, #jump_key
	;mov	 dptr, #jump_cmd
	acall	.print
	mov	r4, #dump_key
	;mov	 dptr, #dump_cmd
	acall	.print
	mov	r4, #intm_key
	;mov	dptr, #intm_cmd
	acall	.print
	mov	r4, #edit_key
	;mov	 dptr, #edit_cmd
	acall	.print
	mov	r4, #clrm_key
	;mov	 dptr, #clrm_cmd
	acall	.print
	mov	r4, #dump_strings_key
	acall	.print
	mov	a, #has_flash
	jz	.no_flash
	mov	r4, #erfr_key
	;mov	 dptr, #erfr_cmd
	acall	.print
.no_flash:
	mov	dptr, #help2txt
	acall	pcstr_h
	acall	find_first
	jnc	.end
.find_ext_loop:
	mov	dpl, #4
	clr	a
	movc	a,@a+dptr
	cjne	a, #254, .next_ext	; only FE is an ext command
	acall	dspace
	inc	dpl
	clr	a
	movc	a,@a+dptr
	acall	cout
	acall	dash_sp
	mov	dpl, #32
	acall	pstr
	acall	newline
.next_ext:
	acall	find.next
	jc	.find_ext_loop
.end:
	ajmp	newline
.print:
	; print 11 standard lines
	; given key in R4 and name in dptr
	acall	dspace
	mov	a, r4
	acall	cout
	acall	dash_sp
	acall	pcstr_h
	ajmp	newline


;---------------------------------------------------------;
; Upload memory as intel hex

upld:
	acall	get_mem
	; assume we've got the beginning address in r3/r2
	; and the final address in r5/r4 (r4=lsb)...

	; print out what we'll be doing
	mov	dptr, #uplds3
	acall	pcstr_h
	mov	a, r3
	acall	phex
	mov	a, r2
	acall	phex
	; mov	 dptr, #uplds4
	acall	pcstr_h
	mov	a, r5
	acall	phex
	mov	a, r4
	acall	phex
	acall	newline

	; need to adjust end location by 1...
	mov	dph, r5
	mov	dpl, r4
	inc	dptr
	mov	r4, dpl
	mov	r5, dph

	mov	dptr, #prompt7
	acall	pcstr_h
	acall	cin
	cjne	a, #esc_char, .no_esc
	ajmp	abort_it
.no_esc:
	acall	newline
	mov	dpl, r2
	mov	dph, r3

.new_line:
	mov	a, r4		; how many more bytes to output??
	clr	c
	subb	a, dpl
	mov	r2, a
	mov	a, r5
	subb	a, dph
	jnz	.do_16b		; if >256 left, then do next 16
	mov	a, r2
	jz	.end		; if we're all done
	anl	a, #11110000b
	jnz	.do_16b		; if >= 16 left, then do next 16
	sjmp	.do_rest	; otherwise just finish it off
.do_16b:
	mov	r2, #16
.do_rest:
	mov	a, #':'		; begin the line
	acall	cout
	mov	a, r2
	acall	phex		; output # of data bytes
	acall	phex16		; output memory location
	mov	a, dph
	add	a, dpl
	add	a, r2
	mov	r3, a		; r3 will become checksum
	clr	a
	acall	phex		; output 00 code for data
.loop:
	clr	a
	movc	a, @a+dptr
	acall	phex		; output each byte
	add	a, r3
	mov	r3, a
	inc	dptr
	djnz	r2, .loop	; do however many bytes we need
	mov	a, r3
	cpl	a
	inc	a
	acall	phex		; and finally the checksum
	acall	newline
;	acall	line_dly
	acall	esc
	jnc	.new_line	; keep working if no esc pressed
	sjmp	abort_it
.end:
	mov	a, #':'
	acall	cout
	clr	a
	acall	phex
	acall	phex
	acall	phex
	inc	a
	acall	phex
	mov	a, #255
	acall	phex
	ajmp	newline2


line_dly:
	; a brief delay between line while uploading, so the
	; receiving host can be slow (i.e. most windows software)
	mov	a, r0
	push	acc
	mov	r0, #line_delay*2
.loop:
	mov	a, th0		; get baud rate const
-	inc	a
	nop
	nop
	jnz	-
	djnz	r0, .loop
	pop	acc
	mov	r0, a
	ret

;---------------------------------------------------------;

get_mem:
	; this thing gets the begin and end locations for
	; a few commands.  If an esc or enter w/ no input,
	; it pops it's own return and returns to the menu
	; (nasty programming, but we need tight code for 4k rom)
	acall	newline2
	mov	dptr, #beg_str
	acall	pcstr_h
	acall	ghex16
	jc	pop_it
	jb	psw.5, pop_it
	push	dph
	push	dpl
	acall	newline
	mov	dptr, #end_str
	acall	pcstr_h
	acall	ghex16
	mov	r5, dph
	mov	r4, dpl
	pop	acc
	mov	r2, a
	pop	acc
	mov	r3, a
	jc	pop_it
	jb	psw.5, pop_it
	ajmp	newline

pop_it:
	pop	acc
	pop	acc
abort_it:
	acall	newline
abort2:
	mov	dptr, #abort
	ajmp	pcstr_h

;---------------------------------------------------------;
; Clear memory

clrm:
	acall	get_mem
	mov	dptr, #sure
	acall	pcstr_h
	acall	cin_filter_h
	acall	upper
	cjne	a, #'Y', abort_it
	acall	newline2
     	; now we actually do it
	mov	dph, r3
	mov	dpl, r2
.loop:
	clr	a
	lcall	smart_wr
	mov	a, r5
	cjne	a, dph, .next
	mov	a, r4
	cjne	a, dpl, .next
	ret
.next:
	inc	dptr
	sjmp	.loop

;---------------------------------------------------------;
; Set new memory location

nloc:
	mov	dptr, #prompt6
	acall	pcstr_h
	acall	ghex16
	jc	abort2
	jb	psw.5, abort2
	acall	dptrtor6r7
	ajmp	newline2

;---------------------------------------------------------;
; Erase flash

erfr:
	acall	newline2
	mov	dptr, #erfr_cmd
	acall	pcstr_h
	mov	a, #','
	acall	cout_sp
	mov	dptr, #sure
	acall	pcstr_h
	acall	cin_filter_h
	acall	upper
	cjne	a, #'Y', abort_it
	acall	newline2
	lcall	erall
	mov	dptr, #erfr_ok
	jnc	.end
	mov	dptr, #erfr_err
.end:
	ajmp	pcstr_h



;---------------------------------------------------------;
; Dump internal memory

intm:
	acall	newline
	mov	r0, #0
.new_line:
	acall	newline
;	cjne	r0, #eintmem, .not_end
;	ajmp	newline
.not_end
	mov	a, r0
	acall	phex
	mov	a, #':'
	acall	cout
.loop:
	acall	space
	mov	a, @r0
	acall	phex
	inc	r0
	mov	a, r0
	anl	a, #00001111b
	jnz	.loop
	cjne	r0, #eintmem, .new_line
	ajmp	newline


; Initializes the memory search and finds the first
; header

find_first:
	; Init a memory search routine
	clr	ioSkip
	mov	dptr, #bmem

; finds the next header in the external memory.
; Input DPTR=point to start search (only MSB used)
; Output DPTR=location of next module
; C=set if a header found, C=clear if no more headers

find:
	mov	dpl, #0
	clr	a
	movc	a, @a+dptr
	cjne	a, #0A5H, .next
	inc	dptr
	clr	a
	movc	a, @a+dptr
	cjne	a, #0E5H, .next
	inc	dptr
	clr	a
	movc	a, @a+dptr
	cjne	a, #0E0H, .next
	inc	dptr
	clr	a
	movc	a, @a+dptr
	cjne	a, #0A5H, .next
	mov	dpl, #0			; found one here!
	setb	c
	ret
.next:
	; Check for the end of memory regions and then increment
	; This is used as an entrypoint to avoid duplicating the increment
	; logic in memory searches.
	mov	a, #(emem >> 8)
	jb	ioSkip, .flash
	cjne	a, dph, .increment	; did we just check the end
	; Go to the flash region
	setb 	ioSkip
	mov	dptr, #bflash
.increment:
	inc	dph			; keep on searching
	sjmp	find
.flash:
	mov	a, #(eflash >> 8)
	cjne	a, dph, .increment
	clr	c
	ret


;**************************************************************
;**************************************************************
;*****							  *****
;*****	     2k page boundry is somewhere near here	  *****
;*****	       (no ajmp or acall past this point)	  *****
;*****							  *****
;**************************************************************
;**************************************************************



;---------------------------------------------------------;
;							  ;
;   Subroutines for memory managment and non-serial I/O	  ;
;							  ;
;---------------------------------------------------------;




; poll the flash rom using it's toggle bit feature
; on D6... and wait until the flash rom is not busy
; dptr must be initialized with the address to read

; flash_wait:
; 	push	b
; 	clr	a
; 	movc	a, @a+dptr
; flwt2:	mov	b, a
; 	inc	r5
; 	clr	a
; 	movc	a, @a+dptr
; 	cjne	a, b, flwt2
; 	pop	b
; 	ret
;
; 	;send the flash enable codes
; flash_en:
; 	mov	dptr, #flash_en1_addr
; 	mov	a, #flash_en1_data
; 	movx	@dptr, a
; 	mov	dptr, #flash_en2_addr
; 	mov	a, #flash_en2_data
; 	movx	@dptr, a
; 	ret


;a routine that writes ACC to into flash memory at DPTR
; assumes that Vpp is active and stable already.
; C is set if error occurs, C is clear if it worked

prgm:
	mov	b, a
	push	ar2
	mov	r2, #25		; try to program 25 times if needed
.loop:
	mov	a, #40h
	push	ie
	clr	ea		; turn off all interrupts
	movx	@dptr, a	; send setup programming command
	mov	a, b
	movx	@dptr, a	; write to the cell
	mov	a, #pgmwait	; now wait for 10us
	djnz	acc, $
	mov	a, #0C0h
	movx	@dptr, a	; send program verify command
	mov	a, #verwait	; wait 6us while it adds margin
	djnz	acc, $
	movc	a, @a+dptr
	pop	ie		; turn interrupts back on
	clr	c
	subb	a, b
	jz	.ok		; note, C is still clear is ACC=0
	djnz	r2, .loop
.bad:
	setb	c		; it gets here if programming failure
.ok:
	clr	a
	movx	@dptr, a	; and go back into read mode
	pop	ar2
	mov	a, b		; restore ACC to original value
	ret

; PM21; a routine that writes ACC to into flash memory at DPTR
; C is set if error occurs, C is clear if it worked


; prgm_pm21:
;	xch	a, r0
; 	push	acc
; 	push	dpl
; 	push	dph
; 	acall	flash_en		; do first step, enable writing
; 	mov	dptr, #flash_wr_addr
; 	mov	a, #flash_wr_data
; 	movx	@dptr, a		; send flash write command
; 	pop	dph
; 	pop	dpl
; 	mov	a, r0
; 	movx	@dptr, a		; write the data
; 	acall	flash_wait		; wait until it's done
; 	clr	a
; 	movc	a, @a+dptr		; read it back
; 	clr	c
; 	xrl	a, r0
; 	jz	prgmend_pm21		; check if data written ok
; 	setb	c
; prgmend_pm21:
;	pop	acc
; 	xch	a, r0
; 	ret

; erase the entire flash rom
; C=1 if failure, C=0 if ok


;routine that erases the whole flash rom!  C=1 if failure, C=0 if ok

erall:
	mov	a, #has_flash
	jz	.error
	mov	dptr, #bflash		; is it already erased ??
.loop:
	clr	a
	movc	a, @a+dptr
	cpl	a
	jnz	.erase			; do actual erase if any byte not 255
	inc	dptr
	mov	a, #((eflash + 1) & 255)
	cjne	a, dpl, .loop
	mov	a, #(((eflash + 1) >> 8) & 255)
	cjne	a, dph, .loop
	; if we get here, the entire chip was already erased,
	; so there is no need to do anything
	clr	c
	ret
.erase:
	mov	dptr, #bflash		; first program to all 00's
.loop_00:
	clr	a
	movc	a, @a+dptr
	jz	.next_00		; don't waste time!
	clr	a
	lcall	prgm			; ok, program this byte
	; if the program operation failed... we should abort because
	; they are all likely to fail and it will take a long time...
	; which give the appearance that the program has crashed,
	; when it's really following the flash rom algorithm
	; correctly and getting timeouts.
	jc	.error
	; mov	a, #'.'
	; lcall	cout
.next_00:
	inc	dptr
	mov	a, #((eflash + 1) & 255)
	cjne	a, dpl, .loop_00
	mov	a, #(((eflash + 1) >> 8) & 255)
	cjne	a, dph, .loop_00		; after this it's all 00's
	mov	dptr, #bflash			; beginning address
	mov	r4, #232			; max # of trials, lsb
	mov	r5, #4				; max # of trials, msb-1
.erase_retry:
	;mov	a, #'#'
	;lcall	cout
	djnz	r4, .erase_cmd
	djnz	r5, .erase_cmd
.error:
	setb	c
	ret					; if it didn't work!
.erase_cmd:
	mov	a, #20h
	mov	c, ea		 		; turn off all interrupts!!
	mov	psw.1, c
	clr	ea
	movx	@dptr, a			; send the erase setup
	movx	@dptr, a			; and begin the erase
	mov	r3, #erwait1
-	mov	r2, #erwait2			; now wait 10ms...
	djnz	r2, $
	djnz	r3, -
.verify:
	mov	a, #0A0h
	movx	@dptr, a			; send erase verify
	mov	r2, #verwait			; wait for 6us
	djnz	r2, $
	clr	a
	movc	a, @a+dptr
	mov	c, psw.1
	mov	ea, c				; turn interrupts back on
	cpl	a
	jnz	.erase_retry			; erase again if not FF
	inc	dptr
	mov	a, #(((eflash + 1) >> 8) & 255)	; verify whole array
	cjne	a, dph, .verify
	mov	a, #((eflash + 1) & 255)
	cjne	a, dpl, .verify
	mov	a, #255
	mov	dptr, #bflash
	movx	@dptr, a			; reset the flash rom
	clr	a
	movx	@dptr, a			; and go back to read mode
	clr	c
	ret

; PM2.1 stuff
; erall_pm21:
; 	mov	dptr, #flash_er2_addr
; 	mov	a, #flash_er2_data
; 	acall	erblock_pm21			; use erblock to send erase all
; 	mov	dptr, #bflash
; erall2_pm21:
;	clr	a
; 	movc	a, @a+dptr			; read back flash memory
; 	cpl	a
; 	jnz	erall_err_pm21			; check if it's really erased
; 	inc	dptr
; 	mov	a, #((eflash+1) & 255)
; 	cjne	a, dpl, erall2_pm21
; 	mov	a, #(((eflash+1) >> 8) & 255)
; 	cjne	a, dph, erall2_pm21
; 	clr	c
; 	ret
; erall_err_pm21:
; 	setb	c
; 	ret
;
;
; 	;send a custom erase command.  This is used by erall,
; 	;and it's intended to be callable from the flash memory
; 	;so that custom block erase code can be implemented
; erblock_pm21:
; 	push	acc
; 	push	dpl
; 	push	dph
; 	call	flash_en		; send flash enable stuff
; 	mov	dptr, #flash_er1_addr
; 	mov	a, #flash_er1_data
; 	movx	@dptr, a		; send erase enable
; 	call	flash_en		; send flash enable stuff
; 	pop	dph
; 	pop	dpl
; 	pop	acc
; 	movx	@dptr, a		; send erase command
; 	jmp	flash_wait



; ************************************
; To make PAULMON2 able to write to other
; types of memory than RAM and flash rom,
; modify this "smart_wr" routine.  This
; code doesn't accept any inputs other
; that the address (dptr) and value (acc),
; so this routine must know which types
; of memory are in what address ranges
; ************************************


; Write to Flash ROM or ordinary RAM.  Carry bit will indicate
; if the value was successfully written, C=1 if not written.


smart_wr:
	push	acc
	push	b
	mov	b, a
	; do we even have a flash rom?
	mov	a, #has_flash
	jz	.ram
	; there is a flash rom, but is this address in it?
	mov	a, dph
	cjne	a, #(eflash >> 8), .is_flash1
	sjmp	.flash
.is_flash1:
	jnc	.ram
	cjne	a, #(bflash >> 8), .is_flash2
	sjmp	.flash
.is_flash2:
	jnc	.flash
	; sjmp	wr_ram
.ram:
	mov	a, b
	movx	@dptr, a		; write the value to memory
	clr	a
	movc	a, @a+dptr		; read it back from code memory
	clr	c
	subb	a, b
	jz	.ok
	movx	a, @dptr		; read it back from data memory
	clr	c
	subb	a, b
	jz	.ok
.bad:
	setb	c
	sjmp	.exit
.ok:
	clr	c
.exit:
	pop	b
	pop	acc
	ret
.flash:
	mov	a, b
	lcall	prgm
	pop	b
	pop	acc
	ret


;---------------------------------------------------------;
;							  ;
;	Power-On initialization code and such...	  ;
;							  ;
;---------------------------------------------------------;

; first the hardware has to get initialized.

intr_return:
	reti

poweron:
	clr	a
	mov	ie, a		; all interrupts off
	mov	ip, a
	mov	psw, #psw_init
	; clear any interrupt status, just in case the user put
	; "ljmp 0" inside their interrupt service code.
	acall	intr_return
	acall	intr_return
	cpl	a
	mov	p0, a
	mov	p1, a
	mov	p2, a
	mov	p3, a
	mov	sp, #stack

; Before we start doing any I/O, a short delay is required so
; that any external hardware which may be in "reset mode" can
; initialize.  This is typically a problem when a 82C55 chip
; is used and its reset line is driven from the R-C reset
; circuit used for the 8051.  Because the 82C55 reset pin
; switches from zero to one at a higher voltage than the 8051,
; any 82C55 chips would still be in reset mode right now...

rst_dly:
	mov	r1, #200	; approx 100000 cycles
-	mov	r2, #249	; 500 cycles
	djnz	r2, $
	djnz	r1, -

; Check for the Erase-on-startup signal and erase Flash ROM
; if it's there.

	mov	a, #has_flash
	jz	skip_erase
	mov	a, #erase_pin
	jz	skip_erase
	mov	r0, #250	; check it 250 times, just to be sure
chk_erase:
	mov	c, erase_pin
	mov	r1, #200
	djnz	r1, $		; short delay
	jc	skip_erase	; skip erase if this bit is not low
	djnz	r0, chk_erase
	lcall	erall		; and this'll delete the flash rom
skip_erase:

; run any user initialization programs in external memory
	mov	b, #249
	acall	stcode

; initialize the serial port, auto baud detect if necessary
	acall	autobaud	; set up the serial port
	; mov	a, th1
	; lcall	phex

; run the start-up programs in external memory.
	mov	b, #253
	acall	stcode

; now print out the nice welcome message

; FIXME - memory hole handling

welcome:
	mov	r0, #24				; 24 newlines
-	lcall	newline
	djnz	r0, -
;	mov	r0, #15				; 15 spaces
;	mov	a, #' '
; -	lcall	cout
;	djnz	r0, -
	mov	dptr, #logon1
	lcall	pcstr
	mov	dptr, #logon2
	lcall	pcstr
	lcall	dir
	mov	r6, #(pgm & 255)
	mov	r7, #(pgm >> 8)
	ljmp	menu

stcode:
	; search for startup routines
	lcall	find_first
	jnc	.end
.loop:
	mov	dpl, #4
	clr	a
	movc	a, @a+dptr
	cjne	a, b, .next	; only startup code if matches B
	push	b
	push	dph
	mov	a, #(.return & 255)
	push	acc
	mov	a, #(.return >> 8)
	push	acc
	mov	dpl, #64
	clr	a
	jmp	@a+dptr		; jump to the startup code
.return:
	pop	dph		; hopefully it'll return to here
	pop	b
.next:
	lcall	find.next
	jc	.loop
.end:
	ret

; to do automatic baud rate detection, we assume the user will
; press the carriage return, which will cause this bit pattern
; to appear on port 3 pin 0 (CR = ascii code 13, assume 8N1 format)
;
;	       0 1 0 1 1 0 0 0 0 1
;	       | |	       | |
; start bit----+ +--lsb	  msb--+ +----stop bit
;
; we'll start timer #1 in 16 bit mode at the transition between the
; start bit and the LSB and stop it between the MBS and stop bit.

; That will give approx the number of cpu cycles for 8 bits.  Divide
; by 8 for one bit and by 16 since the built-in UART takes 16 timer
; overflows for each bit.  We need to be careful about roundoff during
; division and the result has to be inverted since timer #1 counts up.  Of
; course, timer #1 gets used in 8-bit auto reload mode for generating the
; built-in UART's baud rate once we know what the reload value should be.


autobaud:
	ifdef	uart_t2
	; Timer 2 uart

	mov	T2CON, #00110100b		; TRCLK = 1, TCLK = 1, TR2 = 1, rest 0
	mov 	RCAP2H, #0FFH
	mov	RCAP2L, #baud_rate
	mov	SCON, #01011010B		; SM1, REN, TB8, TI
	ajmp	.end

	elseif
	; Timer 1 based uart

	; Disabled for now, 16MHz and T1 as baud rate generator isn't a good combo
	; TODO: take autobaud from basic-52 ??
	mov	a, #baud_const			; skip if user supplied baud rate constant
	jnz	.jmp_end
	mov	a, baud_save + 3			; is there a value from a previous boot?
	xrl	baud_save + 2, #01010101b
	xrl	baud_save + 1, #11001100b
	xrl	baud_save + 0, #00011101b
	cjne	a, baud_save + 2, .start
	cjne	a, baud_save + 1, .start
	cjne	a, baud_save + 0, .start

	endif
.jmp_end:
	ajmp	.end
.start:
	; wait for inactivity
	mov	pcon, #080H			; configure uart, fast baud
	mov	scon, #042H			; configure uart, but receive disabled
	mov	tmod, #011H			; get timers ready for action (16 bit mode)
	clr	a
	mov	tcon, a
	mov	tl0, a
	mov	th0, a
	mov	tl1, a
	mov	th1, a

	; make sure there is no activity on the line
	; before we actually begin looking for the carriage return
	mov	r0, #200
.init_loop:
	mov	r1, #30
-	jnb	p3.0, .start
	djnz	r1, -
	djnz	r0, .init_loop
.start_bit:
	; look for the bits of the carriage return
	jb	p3.0, .start_bit		; wait for start bit
	jb	p3.0, .start_bit
	jb	p3.0, .start_bit		; check it a few more times to make
	jb	p3.0, .start_bit		; sure we don't trigger on some noise
	jb	p3.0, .start_bit
-	jnb	p3.0, -				; wait for bit #0 to begin
	setb	tr1				; and now we're timing it
-	jb	tf1, .start			; check for timeout while waiting
	jb	p3.0, -				; wait for bit #1 to begin
-	jb	tf1, .start			; check for timeout while waiting
	jnb	p3.0, -				; wait for bit #2 to begin
-	jb	tf1, .start			; check for timeout while waiting
	jb	p3.0, -				; wait for bit #4 to begin
	setb	tr0				; start timing last 4 bits
-	jb	tf1, .start			; check for timeout while waiting
	jnb	p3.0, -				; wait for stop bit to begin
	clr	tr1				; stop timing (both timers)
	clr	tr0

	jb	tf1, .start			; check for timeout one last time

	; compute the baud rate based on timer1
	mov	a, tl1
	rlc	a
	mov	b, a
	mov	a, th1
	rlc	a
	jc	.start				; error if timer0 > 32767
	mov	c, b.7
	addc	a, #0
	cpl	a
	inc	a				; now a has the value to load into th1
	jz	.start				; error if baud rate too fast

	; after we get the carriage return, we need to make sure there
	; isn't any "crap" on the serial line, as there is in the case
	; were we get the letter E (and conclude the wrong baud rate).
	; unfortunately the simple approach of just looking at the line
	; for silence doesn't work, because we have to accept the case
	; where the user's terminal emulation is configured to send a
	; line feed after the carriage return.  The best thing to do is
	; use the uart and look see if it receives anything

	mov	th1, a				; config timer1
	mov	tl1, #255			; start asap!
	mov	tmod, #021H			; autoreload mode
	setb	ren				; turn on the uart
	setb	tr1				; turn on timer1 for its clock

	mov	a, th1
	cpl	a
	inc	a
	mov	r1, a
-	mov	r0, #255
-	djnz	r0, -
	djnz	r1, --
	jnb	ri, .calc_baud
	; if we got here, there was some stuff after the carriage
	; return, so we'll read it and see if it was the line feed
	clr	ri
	mov	a, sbuf
	anl	a, #01111111b
	add	a, #246
	jz	.calc_baud			; ok if 0A, the line feed character
	add	a, #5
	jz	.calc_baud			; of if 05, since we may have missed start bit
.jmp_start:
	ljmp	.start
.calc_baud:
	; compute the baud rate based on timer0, check against timer1 value
	mov	a, tl0
	rlc	a
	mov	r0, a
	mov	a, th0
	rlc	a
	mov	r1, a
	jc	.jmp_start			; error if timer0 > 32767
	mov	a, r0
	rlc	a
	mov	b, a
	mov	a, r1
	rlc	a
	mov	c, b.7
	addc	a, #0
	jz	.jmp_start			; error if baud too fast!
	cpl	a
	inc	a
	cjne	a, th1, .jmp_start
	; acc has th1 value at this point
.end:
	mov	baud_save + 3, a
	mov	baud_save + 2, a			; store the baud rate for next warm boot.
	mov	baud_save + 1, a
	mov	baud_save + 0, a
	xrl	baud_save + 2, #01010101b
	xrl	baud_save + 1, #11001100b
	xrl	baud_save + 0, #00011101b
	mov	th1, a
	mov	tl1, a
	mov	tmod, #021H			; set timer #1 for 8 bit auto-reload
	mov	pcon, #080H			; configure built-in uart
	mov	scon, #052H
	setb	tr1				; start the baud rate timer
	ret



;---------------------------------------------------------;
;							  ;
;     More subroutines, but less frequent used, so	  ;
;     they're down here in the second 2k page.		  ;
;							  ;
;---------------------------------------------------------;



; this twisted bit of code looks for escape sequences for
; up, down, left, right, pageup, and pagedown, as well
; as ordinary escape and ordinary characters.  Escape
; sequences are required to arrive with each character
; nearly back-to-back to the others, otherwise the characters
; are treated as ordinary user keystroaks.  cin_filter
; returns a single byte when it sees the multi-byte escape
; sequence, as shown here.

; return value	 key		escape sequence
;   11 (^K)	 up		1B 5B 41
;   10 (^J)	 down		1B 5B 42
;   21 (^U)	 right		1B 5B 43
;    8 (^H)	 left		1B 5B 44
;   25 (^Y)	 page up	1B 5B 35 7E
;   26 (^Z)	 page down	1B 5B 36 7E

cin_filter:
	jnb	ri, +
	lcall	cin
	cjne	a, #esc_char, .end
	; if esc was already in sbuf, just ignore it
+	lcall	cin
	cjne	a, #esc_char, .end
.loop:
	acall	cinf_wait
	jb	ri, .get_chr
	mov	a, #esc_char
	ret				; an ordinary ESC

.get_chr:
	; if we get here, it's a control code, since a character
	; was received shortly after receiving an ESC character
	lcall	cin
	cjne	a, #'[', .consume
	acall	cinf_wait
	jnb	ri, cin_filter
	lcall	cin
.check_up:
	cjne	a, #'A', .check_down
	mov	a, #11
	ret
.check_down:
	cjne	a, #'B', .check_right
	mov	a, #10
	ret
.check_right:
	cjne	a, #'C', .check_left
	mov	a, #21
	ret
.check_left:
	cjne	a, #'D', .check_page_up
	mov	a, #8
	ret
.check_page_up:
	cjne	a, #035H, .check_page_down
	sjmp	.handle_page
.check_page_down:
	cjne	a, #036H, .jmp_consume
	sjmp	.handle_page
.jmp_consume:
	sjmp	.consume		; unknown escape sequence

.handle_page:
	; when we get here, we've got the sequence for pageup/pagedown
	; but there's one more incoming byte to check...
	push	acc
	acall	cinf_wait
	jnb	ri, .restart
	lcall	cin
	cjne	a, #07EH, .not_page
	pop	acc
	add	a, #228
.end:
	ret
.restart:
	pop	acc
	sjmp	cin_filter
.not_page:
	pop	acc
	; unrecognized escape... eat up everything that's left coming in
	; quickly, then begin looking again
.consume:
	acall	cinf_wait
	jnb	ri, cin_filter
	lcall	cin
	cjne	a, #esc_char, .consume
	sjmp	.loop

; this thing waits for a character to be received for approx
; 4 character transmit time periods.  It returns immedately
; or after the entire wait time.	 It does not remove the character
; from the buffer, so ri should be checked to see if something
; actually did show up while it was waiting

cinf_wait:
	mov	a, r2
	push	acc
	mov	r2, #char_delay * 5
-	mov	a, th0
-	jb	ri, .end
	inc	a
	jnz	-
	djnz	r2, --
.end:
	pop	acc
	mov	r2, a
	ret


pint8u:
	; prints the unsigned 8 bit value in Acc in base 10
	push	b
	push	acc
	sjmp	pint8.positive

pint8:
	; prints the signed 8 bit value in Acc in base 10
	push	b
	push	acc
	jnb	acc.7, .positive
	mov	a, #'-'
	lcall	cout
	pop	acc
	push	acc
	cpl	a
	add	a, #1
.positive:
	mov	b, #100
	div	ab
	setb	f0
	jz	.skip_digit
	clr	f0
	add	a, #'0'
	lcall	cout
.skip_digit:
	mov	a, b
	mov	b, #10
	div	ab
	jnb	f0, +
	jz	.end
+	add	a, #'0'
	lcall	cout
.end:
	mov	a, b
	add	a, #'0'
	lcall	cout
	pop	acc
	pop	b
	ret



	; print 16 bit unsigned integer in DPTR, using base 10.
	; warning, destroys r2, r3, r4, r5, psw.5
pint16u:
	push	acc
	mov	a, r0
	push	acc
	clr	psw.5
	mov	r2, dpl
	mov	r3, dph
.digit1:
	mov	r4, #16			; ten-thousands digit
	mov	r5, #39
	acall	pint16x
	jz	.digit2
	add	a, #'0'
	lcall	cout
	setb	psw.5
.digit2:
	mov	r4, #232		; thousands digit
	mov	r5, #3
	acall	pint16x
	jnz	.out2
	jnb	psw.5, .digit3
.out2:
	add	a, #'0'
	lcall	cout
	setb	psw.5
.digit3:
	mov	r4, #100		; hundreds digit
	mov	r5, #0
	acall	pint16x
	jnz	.out3
	jnb	psw.5, .digit4
.out3:
	add	a, #'0'
	lcall	cout
	setb	psw.5
.digit4:
	mov	a, r2			; tens digit
	mov	r3, b
	mov	b, #10
	div	ab
	jnz	.out4
	jnb	psw.5, .digit5
.out4:
	add	a, #'0'
	lcall	cout
.digit5:
	mov	a, b			; and finally the ones digit
	mov	b, r3
	add	a, #'0'
	lcall	cout

	pop	acc
	mov	r0, a
	pop	acc
	ret

; ok, it's a cpu hog and a nasty way to divide, but this code
; requires only 21 bytes!  Divides r2-r3 by r4-r5 and leaves
; quotient in r2-r3 and returns remainder in acc.  If Intel
; had made a proper divide, then this would be much easier.

pint16x:
	mov	r0, #0
.loop:
	inc	r0
	clr	c
	mov	a, r2
	subb	a, r4
	mov	r2, a
	mov	a, r3
	subb	a, r5
	mov	r3, a
	jnc	.loop
	dec	r0
	mov	a, r2
	add	a, r4
	mov	r2, a
	mov	a, r3
	addc	a, r5
	mov	r3, a
	mov	a, r0
	ret


dump_strings:
	mov	r2, #128
	lcall	newline
.loop:
	mov	a, r2
	lcall	pint8u
	acall	decomp
	inc	r2
	lcall	newline
	cjne	r2, #0, .loop
	ret

; pcstr prints the compressed strings.  A dictionary of 128 words is
; stored in 4 bit packed binary format.	When pcstr finds a byte in
; a string with the high bit set, it prints the word from the dictionary.
; A few bytes have special functions and everything else prints as if
; it were an ordinary string.

; special codes for pcstr:
;    0 = end of string
;   13 = CR/LF
;   14 = CR/LF and end of string
;   31 = next word code should be capitalized

pcstr:
	push	acc
	mov	a, r0
	push	acc
	mov	a, r1
	push	acc
	mov	a, r4
	push	acc
	setb	psw.1
	setb	psw.5
.loop:
	clr	a
	movc	a, @a+dptr
	inc	dptr
	jz	.end
	jb	acc.7, .call_decomp
	anl	a, #07FH
.check_cr:
	cjne	a, #13, .check_capital
	lcall	newline
	setb	psw.1
	sjmp	.loop
.check_capital:
	cjne	a, #31, .check_cr_end
	clr	psw.5
	sjmp	.loop
.check_cr_end:
	cjne	a, #14, .next
	lcall	newline
	sjmp	.end
.next:
	clr	psw.1
	lcall	cout
	sjmp	.loop
.end:
	pop	acc
	mov	r4, a
	pop	acc
	mov	r1, a
	pop	acc
	mov	r0, a
	pop	acc
	ret
.call_decomp:
	call	decomp
	sjmp	.loop

; dcomp actually takes care of printing a word from the dictionary

; dptr = position in packed words table
; r4=0 if next nibble is low, r4=255 if next nibble is high

; Register usage:
;	Destroys ACC.7, psw.1, psw.5, r0, r1, r4
;	A contains input byte
;	dptr unaffected
decomp:
	anl	a, #07FH
	mov	r0, a			; r0 counts which word
	jb	psw.1, +		; avoid leading space if first word
	lcall	space
+	clr	psw.1
	push	dpl
	push	dph
	mov	dptr, #words
	mov	r4, #0
	mov	a, r0
	jz	.print
	; here we must seek past all the words in the table
	; that come before the one we're supposed to print
	mov	r1, a
.loop:
	acall	get_next_nibble
	jnz	.loop
	; when we get here, a word has been skipped... keep doing
	; this until we're pointing to the correct one
	djnz	r1, .loop
.print:
	; now we're pointing to the correct word, so all we have
	; to do is print it out
	acall	get_next_nibble
	jz	.end
	cjne	a, #15, .common
	; the character is one of the 12 least commonly used
	acall	get_next_nibble
	inc	a
	movc	a, @a+pc
	sjmp	.case
	db	"hfwgybxvkqjz"
.common:
	; the character is one of the 14 most commonly used
	inc	a
	movc	a, @a+pc
	sjmp	.case
	db	"etarnisolumpdc"
.case:
	; decide if it should be uppercase or lowercase
	mov	c, psw.5
	mov	acc.5, c
	setb	psw.5
	cjne	r0, #20, +
	clr	acc.5
+	cjne	r0, #12, +
	clr	acc.5
+	lcall	cout
	sjmp	.print
.end:
	pop	dph
	pop	dpl
	ret

get_next_nibble:
	; ...and update dptr and r4, of course
	clr	a
	movc	a, @a+dptr
	cjne	r4, #0, .high
.low
	mov	r4, #255
	anl	a, #00001111b
	ret
.high:
	mov	r4, #0
	inc	dptr
	swap	a
	anl	a, #00001111b
	ret


;---------------------------------------------------------;
;							  ;
;	 Here begins the data tables and strings	  ;
;							  ;
;---------------------------------------------------------;

; this is the dictionary of 128 words used by pcstr.
; see cstr.inc for the string dictionary

words:
	db	082H, 090H, 0E8H, 023H, 086H, 005H, 04CH, 0F8H
	db	044H, 0B3H, 0B0H, 0B1H, 048H, 05FH, 0F0H, 011H
	db	07FH, 0A0H, 015H, 07FH, 01CH, 02EH, 0D1H, 040H
	db	05AH, 050H, 0F1H, 003H, 0BFH, 0BAH, 00CH, 02FH
	db	096H, 001H, 08DH, 03FH, 095H, 038H, 00DH, 06FH
	db	05FH, 012H, 007H, 071H, 00EH, 056H, 02FH, 048H
	db	03BH, 062H, 058H, 020H, 01FH, 076H, 070H, 032H
	db	024H, 040H, 0B8H, 040H, 0E1H, 061H, 08FH, 001H
	db	034H, 00BH, 0CAH, 089H, 0D3H, 0C0H, 0A3H, 0B9H
	db	058H, 080H, 004H, 0F8H, 002H, 085H, 060H, 025H
	db	091H, 0F0H, 092H, 073H, 01FH, 010H, 07FH, 012H
	db	054H, 093H, 010H, 044H, 048H, 007H, 0D1H, 026H
	db	056H, 04FH, 0D0H, 0F6H, 064H, 072H, 0E0H, 0B8H
	db	03BH, 0D5H, 0F0H, 016H, 04FH, 056H, 030H, 06FH
	db	048H, 002H, 05FH, 0A8H, 020H, 01FH, 001H, 076H
	db	030H, 0D5H, 060H, 025H, 041H, 0A4H, 02CH, 060H
	db	005H, 06FH, 001H, 03FH, 026H, 01FH, 030H, 007H
	db	08EH, 01DH, 0F0H, 063H, 099H, 0F0H, 042H, 0B8H
	db	020H, 01FH, 023H, 030H, 002H, 07AH, 0D1H, 060H
	db	02FH, 0F0H, 0F6H, 005H, 08FH, 093H, 01AH, 050H
	db	028H, 0F0H, 082H, 004H, 06FH, 0A3H, 00DH, 03FH
	db	01FH, 051H, 040H, 023H, 001H, 03EH, 005H, 043H
	db	001H, 07AH, 001H, 017H, 064H, 093H, 030H, 02AH
	db	008H, 08CH, 024H, 030H, 099H, 0B0H, 0F3H, 019H
	db	060H, 025H, 041H, 035H, 009H, 08EH, 0CBH, 019H
	db	012H, 030H, 005H, 01FH, 031H, 01DH, 004H, 014H
	db	04FH, 076H, 012H, 004H, 0ABH, 027H, 090H, 056H
	db	001H, 02FH, 0A8H, 0D5H, 0F0H, 0AAH, 026H, 020H
	db	05FH, 01CH, 0F0H, 0F3H, 061H, 0FEH, 001H, 041H
	db	073H, 001H, 027H, 0C1H, 0C0H, 084H, 08FH, 0D6H
	db	001H, 087H, 070H, 056H, 04FH, 019H, 070H, 01FH
	db	0A8H, 0D9H, 090H, 076H, 002H, 017H, 043H, 0FEH
	db	001H, 0C1H, 084H, 00BH, 015H, 07FH, 002H, 08BH
	db	014H, 030H, 08FH, 063H, 039H, 06FH, 019H, 0F0H
	db	011H, 0C9H, 010H, 06DH, 002H, 03FH, 091H, 009H
	db	07AH, 041H, 0D0H, 0BAH, 00CH, 01DH, 039H, 05FH
	db	007H, 0F2H, 011H, 017H, 020H, 041H, 06BH, 035H
	db	009H, 0F7H, 075H, 012H, 00BH, 0A7H, 0CCH, 048H
	db	002H, 03FH, 064H, 012H, 0A0H, 00CH, 027H, 0E3H
	db	09FH, 0C0H, 014H, 077H, 070H, 011H, 040H, 071H
	db	021H, 0C0H, 068H, 025H, 041H, 0F0H, 062H, 07FH
	db	0D1H, 0D0H, 021H, 0E1H, 062H, 058H, 0B0H, 0F3H
	db	005H, 01FH, 073H, 030H, 077H, 0B1H, 06FH, 019H
	db	0E0H, 019H, 043H, 0E0H, 058H, 02FH, 0F6H, 0A4H
	db	014H, 0D0H, 023H, 003H, 0FEH, 031H, 0F5H, 014H
	db	030H, 099H, 0F8H, 003H, 03FH, 064H, 022H, 051H
	db	060H, 025H, 041H, 02FH, 0E3H, 001H, 056H, 027H
	db	093H, 009H, 0FEH, 011H, 0FEH, 079H, 0BAH, 060H
	db	075H, 042H, 0EAH, 062H, 058H, 0A0H, 0E5H, 01FH
	db	053H, 04FH, 0D1H, 0C0H, 0A3H, 009H, 042H, 053H
	db	0F7H, 012H, 004H, 062H, 01BH, 030H, 0F5H, 005H
	db	0F7H, 069H, 00CH, 035H, 01BH, 070H, 082H, 02FH
	db	02FH, 014H, 04FH, 051H, 0C0H, 064H, 025H, 000H

; STR
; special codes for pcstr:
;    0 = end of string
;   13 = CR/LF
;   14 = CR/LF and end of string
;   31 = space, next word code should be capitalized
;   32 = space

logon1:		; "Welcome to PAULMON2 v3.0, by Paul Stoffregen and Vesa-Pekka Palmu\n\n"
	db	"Welcome", c_to, c_paulmon,"2 v3.0, by", 31, c_paul, 31, c_stoffregen
	db	" and Vesa-Pekka Palmu", 13, 14

logon2:		; "See PAULMON2.DOC, PAULMON2.EQU and PAULMON2.HDR for more information.\n"
	db	"See", c_paulmon, "2.DOC,", c_paulmon, "2.EQU", c_and
	db	c_paulmon, "2.HDR", c_for, c_more, c_information, ".", 14

abort:		; " Command Abort!\n\n"
	db	" ", 31, c_command, 31, c_abort, "!", 13, 14

prompt1:	; "PAULMON2 Loc:"
	db	c_paulmon, "2 Loc:",0

prompt2:
	db	" >", 160				; must follow after prompt1

prompt3:	; "run xxx program ("
	db	c_run, c_which, c_program, '(', 0

prompt4:	; "), or ESC to quit:"
	db	"),", c_or, c_esc, c_to, c_quit, ": ", 0

prompt5:	; "No program headers found in memory, use JUMP to run your program"
	db	31, c_no, c_program, c_header, "s", c_found, c_in, c_memory, ","
	db	c_use, " JUMP", c_to, c_run, c_you,"r", c_program, 13, 14

prompt6:	; "\n\nNew memory location: "
	db	13, 13, 31, c_new, c_memory, c_location, ": ", 0

prompt7:	; "Press any key: "
	db	31, c_press, c_any, " key: ",0

prompt8:	; "\n\nJump to memory location ("
	db	13, 13, 31, c_jump, c_to, c_memory, c_location, " (", 0

prompt9:	; "\n\nProgram Name"
	db	13, 13, 31, c_program, 31, c_name, 0
prompt9b:	; "Location      Type"
	db	 31, c_location, 32, 32, 32, 32, 32, 31, c_type, 14	; must follow prompt9

prompt10:	; ") New value:"
	db	") ", 31, c_new, 31, c_value,": ",0

beg_str:	; "First Location:"
	db	"First", 31, c_location, ": ",0

end_str:	; "Last Location:  "
	db	"Last", 31, c_location, ":", 32, 32, 0

sure:		; "Are you sure?"
	db	31, c_are, c_you, " sure?", 0

edits1:		; "\n\nEditing external ram, ESC to quit\n"
	db	13, 13, 31, c_editing, c_external, c_ram, ",", c_esc, c_to, c_quit, 14

edits2:		; "  Editing complete, this location xxx\n\n"
	db	"  ", 31, c_editing, c_complete, ",", c_this, c_location, c_unchanged, 13, 14

dnlds1:		; "\n\nBegin ascii transfer of Intel hex file, or ESC to abort\n\n"
	db	13, 13, 31, c_begin, " ascii", c_transfer, c_of, 31, c_intel, c_hex, c_file
	db	",", c_or, c_esc, c_to, c_abort, 13, 14

dnlds2:		; "\nDownload aborted\n\n"
	db	13, 31, c_download, c_abort, "ed", 13, 14

dnlds3:		; "\nDownload completed."
	db	13, 31, c_download, c_complete, "d", 13, 14

dnlds4:		; "Summary:"
	db	"Summary:", 14

dnlds5:		; " lines received"
	db	" ", c_line, "s", c_receive, "d", 14

dnlds6a:	; " bytes received"
	db	" ", c_bytes, c_receive, "d", 14

dnlds6b:	; " bytes written"
	db	" ", c_bytes, " written", 14

dnlds7:		; "Errors:\n"
	db	31, c_errors, ":", 14

dnlds8:		; " bytes unable to write\n"
	db	" ", c_bytes, " unable", c_to, " write", 14

dnlds9:		; "  bad checksums\n"
	db	32, 32, "bad", c_checksum, "s", 14

dnlds10:	; " unexpected begin of line\n"
	db	" ", c_unexpected, c_begin, c_of, c_line, 14

dnlds11:	; " unexpected hex digits\n"
	db	" ", c_unexpected, c_hex, c_digits, 14

dnlds12:	; " unexpected non hex digits\n"
	db	" ", c_unexpected, " non", c_hex, c_digits,14

dnlds13:	; "No errors detected\n\n"
	db	31, c_no, c_errors," detected", 13, 14

runs1:		; "\nrunning program:\n\n"
	db	13, c_run, "ning", c_program, ":", 13, 14

uplds3:		; "\n\nSending Intel hex file from  "
	db	13, 13, "Sending", 31, c_intel, c_hex, c_file, c_from, 32, 32, 0

uplds4:		; " to  "
	db	" ", c_to, 32, 32, 0				; must follow uplds3

help1txt:	; "\n\nStandard Commands\n"
	db	13, 13, "Standard", 31, c_command, "s", 14

help2txt:	; "User Installed Commands\n"
	db	31, c_user, 31, c_install, "ed", 31, c_command, "s", 14

type1:		; "External command"
	db	31, c_external, c_command, 0

type2:		; "Program"
	db	31, c_program, 0

type4:
	db	31, c_start, 31, c_up, 31, c_code, 0

type5:		; "???"
	db	"???", 0

help_cmd2:	; "Help"
	db	31, c_help, 0

; these 12 _cmd string must be in order

help_cmd:	; "This help list"
	db	31, c_this, c_help, c_list, 0

dir_cmd:	; "List programs"
	db	31, c_list, c_program, "s", 0

run_cmd:	; "Run program"
	db	31, c_run, c_program, 0

dnld_cmd:	; "Download"
	db	31, c_download, 0

upld_cmd:	; "Upload"
	db	31, c_upload, 0

nloc_cmd:	; "New locatnon"
	db	31, c_new, c_location, 0

jump_cmd:	; "Jump to memory location"
	db	31, c_jump, c_to, c_memory, c_location, 0

dump_cmd:	; "Hex dump external memory"
	db	31, c_hex, c_dump, c_external, c_memory, 0

intm_cmd:	; "Hex dump internal memory"
	db	31, c_hex, c_dump, c_internal, c_memory, 0

edit_cmd:	; "Editing external ram"
	db	31, c_Editing, c_external, c_ram, 0

clrm_cmd:	; "Clear memory"
	db	31, c_clear, c_memory, 0

dump_strings_cmd:
		; "Dump strings"
	db	31, c_dump, " strings", 0

erfr_cmd:	; "Erase flash rom"
	db	31, c_erase, c_flash, c_rom, 0

erfr_ok:	; "Flash rom erased\n\n"
	db	31, c_flash, c_rom, c_erase, 'd', 13, 14

erfr_err:	; "Unexpected errors\n\n"
	db	31, c_unexpected, c_errors, 13, 14
